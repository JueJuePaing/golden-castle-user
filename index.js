/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import { setItem } from './src/utils/appStorage';
import firebase from '@react-native-firebase/app';
import PushNotification from "react-native-push-notification";

// const RNfirebaseConfig = {
//   apiKey: 'AIzaSyCsXTpeIG4UNskZUvaw0ZmF5pIHBKgZZ_A',
//   authDomain: "lottery-dcaa3.firebaseapp.com",
//   projectId: "lottery-dcaa3",
//   storageBucket: "lottery-dcaa3.appspot.com",
//   messagingSenderId: '409576777414',
//   databaseURL: 'https://lottery-dcaa3.firebaseio.com',
//   appId: "1:409576777414:android:c3b0ec493cf3132e6719c7"
// };

const RNfirebaseConfig = {
  apiKey: 'AIzaSyDrdoEgu86H6RskH3vFMHJI3Ero089kW0o',
  authDomain: "shwe-taik.firebaseapp.com",
  projectId: "shwe-taik",
  storageBucket: "shwe-taik.appspot.com",
  messagingSenderId: '409576777414',
  databaseURL: 'https://shwe-taik.firebaseio.com',
  appId: "1:556475852937:android:eaad3e4d4d6cbbb70b7e58"
};

if (firebase.apps.length === 0) {
  firebase.initializeApp(RNfirebaseConfig )
}

PushNotification.configure({
  // (optional) Called when Token is generated (iOS and Android)
  onRegister: function (token) {
    setItem('@fcmToken', token.token);
  },

  // (required) Called when a remote is received or opened, or local notification is opened
  onNotification: function (notification) {
    console.log("onNotification >> ", notification)
    if (notification && notification.data && notification.data.title && notification.data.message) {
      displayNotification(notification.data.title, notification.data.message);
    }
  },

  // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
  onAction: function (notification) {
    console.log("ACTION:", notification.action);
  },

  // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
  onRegistrationError: function(err) {
    console.error(err.message, err);
  },

  // IOS ONLY (optional): default: all - Permissions to register.
  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },

  popInitialNotification: true,

  requestPermissions: true,
});

const displayNotification = (title, desc) => {

  const options = {
    soundName: 'default',
    playSound: true
  }

  PushNotification.localNotification({

    channelId : "fcm_fallback_notification_channel",
    autoCancel: true,
    largeIcon: options.largeIcon || 'ic_launcher',
    smallIcon: options.smallIcon || 'ic_launcher',
    bigText: desc || '',
    subText: title || '',
    vibrate: options.vibrate || true,
    vibration: options.vibration || 300,
    priority: options.priority || 'high',
    importance: options.importance || 'high',
    
    title: title || '',
    message: desc || '',
    playSound: true,
    //soundName: 'default',
    userInteraction: true,
    invokeApp: true
  })
}

const MyHeadlessTask = async () => {
  console.log('Receiving Background actions!!');
};

AppRegistry.registerHeadlessTask('BGActions', () => MyHeadlessTask);
AppRegistry.registerComponent(appName, () => App);
