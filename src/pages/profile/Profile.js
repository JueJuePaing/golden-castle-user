import React, {
    useEffect,
    useState,
    useContext,
    useCallback
} from 'react';
import {
    Linking,
    DeviceEventEmitter,
    View,
    Text,
    Alert,
    TouchableOpacity,
    Image,
    ScrollView,
    StatusBar,
    ToastAndroid,
    RefreshControl
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Entypo from "react-native-vector-icons/Entypo";
import AntDesign from 'react-native-vector-icons/AntDesign'
import Feather from 'react-native-vector-icons/Feather'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import FontAwesome from "react-native-vector-icons/FontAwesome"
import MaterialIcons from "react-native-vector-icons/MaterialIcons"
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"

import { useLocal } from '../../hook/useLocal';
import { fetchPostByToken } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { setItem } from '../../utils/appStorage';
import { encryptData } from '../../utils/encryptData';
import { decryptData } from '../../utils/decryptData';

import { Context } from '../../context/Provider';
import { deleteMultiItems } from '../../utils/appStorage';

import styles from './Style';
import Crown from '../../assets/icons/Crown';
import Youtube from '../../assets/icons/Youtube';
import ProfileMenu from '../../components/profile/ProfileMenu';
import Language from '../../components/language/Language';
import LoginModal from '../auth/Login';
import Terms from '../auth/Terms';
import SessionExpired from '../../components/modal/SessionExpired';

const Profile = ({navigation}) => {

    const local = useLocal();


const SETTINGS_MENUS = [
    {
        id : 1,
        title: local.general,
        sub_menus: [
            {
                id: 12,
                title: local.changePassword,
                icon: <Feather 
                    name="edit"
                    size={hp(2)}
                    color='#ff9700' />
            },
            {
                id: 13,
                title: local.language,
                icon:   <FontAwesome
                    name="language"
                    size={hp(2)}
                    color='#ff9700' />
            },
            {
                id: 14,
                title: local.recharge,
                icon:   <FontAwesome5
                    name="coins"
                    size={hp(2)}
                    color='#ff9700' />
            },
            {
                id: 15,
                title: local.withdraw,
                icon:   <MaterialCommunityIcons
                    name="cash-refund"
                    size={hp(2.2)}
                    color='#ff9700' />
            },
        ]
    },
    {
        id : 2,
        title: local.aboutAndTerms,
        sub_menus: [
            {
                id: 21,
                title: local.aboutUs,
                icon: <AntDesign 
                    name="infocirlce"
                    size={hp(2)}
                    color='#ff9700' />
            },
            {
                id: 22,
                title: local.contactUs,
                icon: <MaterialIcons 
                    name="contact-phone"
                    size={hp(2)}
                    color='#ff9700' />
            },
            {
                id: 30,
                title: local.contactAgent,
                icon: <FontAwesome5 
                    name="phone-alt"
                    size={hp(1.8)}
                    color='#ff9700' />
            },
            // {
            //     id: 23,
            //     title: local.rateUs,
            //     icon: <MaterialIcons 
            //         name="star-rate"
            //         size={hp(2.5)}
            //         color='#ff9700' />
            // },
            {
                id: 24,
                title: local.terms,
                icon: <Feather 
                    name="file-text"
                    size={hp(2)}
                    color='#ff9700' />
            },
            {
                id: 50,
                title: local.privacyAndPolicy,
                icon: <MaterialIcons 
                    name="privacy-tip"
                    size={hp(2.2)}
                    color='#ff9700' />
            },
        ]
    },
    {
        id : 3,
        title: local.helpAndSupport,
        sub_menus: [
            {
                id: 31,
                title: local.faq,
                icon: <AntDesign 
                    name="questioncircle"
                    size={hp(2)}
                    color='#ff9700' />
            },
            {
                id: 32,
                title: local.feedback,
                icon: <MaterialIcons 
                    name="rate-review"
                    size={hp(2)}
                    color='#ff9700' />
            },
            {
                id: 33,
                title: local.deleteAccountRequest,
                icon: <MaterialCommunityIcons 
                    name="delete-empty"
                    size={hp(2.2)}
                    color='#ff9700' />
            }
        ]
    }
]


const SETTINGS_MENUS_UNKNOWN = [
    {
        id : 1,
        title: 'General',
        sub_menus: [
            {
                id: 13,
                title: local.language,
                icon:   <FontAwesome
                    name="language"
                    size={hp(2)}
                    color='#ff9700' />
            },
        ]
    },
    {
        id : 2,
        title: 'About & Terms',
        sub_menus: [
            {
                id: 21,
                title: local.aboutUs,
                icon: <AntDesign 
                    name="infocirlce"
                    size={hp(2)}
                    color='#ff9700' />
            },
            {
                id: 22,
                title: local.contactUs,
                icon: <FontAwesome5 
                    name="phone-alt"
                    size={hp(1.8)}
                    color='#ff9700' />
            },
            // {
            //     id: 23,
            //     title: local.rateUs,
            //     icon: <MaterialIcons 
            //         name="star-rate"
            //         size={hp(2.5)}
            //         color='#ff9700' />
            // },
            {
                id: 24,
                title: local.terms,
                icon: <Feather 
                    name="file-text"
                    size={hp(2)}
                    color='#ff9700' />
            },
            {
                id: 50,
                title: local.privacyAndPolicy,
                icon: <MaterialIcons 
                    name="privacy-tip"
                    size={hp(2.2)}
                    color='#ff9700' />
            },
        ]
    },
    {
        id : 3,
        title: 'Help & Support',
        sub_menus: [
            {
                id: 31,
                title: local.faq,
                icon: <AntDesign 
                    name="questioncircle"
                    size={hp(2)}
                    color='#ff9700' />
            }
        ]
    }
]


    const { 
        changeUserInfo,
        changeLang,
        userInfo,
        profileInfo,
        lang,
        changeProfileInfo
    } = useContext(Context);

    const [
        refreshing,
        setRefreshing
      ] = useState(false);
    const [
        expired,
        setExpired
    ] = useState(false);
    const [
        defaultTab,
        setDefaultTab
    ] = useState(1);
    const [
        showLogin,
        setShowLogin
    ] = useState(false);
    const [
        showTerms,
        setShowTerms
    ] = useState(false);
    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        showLan,
        setShowLan
    ] = useState(false);
    const [
        profileData,
        setProfileData
    ] = useState();

    const onRefresh = useCallback(() => {
    
        const reloadData = async () => {
            setRefreshing(true);
            getProfile(true);
        };
    
        reloadData();
      }, [userInfo]);

    useEffect(() => {
        if (profileInfo) {
            setProfileData(profileInfo)
        } else {
            getProfile(false);
        }
    }, [profileInfo]);

    useEffect(()=> {
        const eventEmitter = DeviceEventEmitter.addListener(
            'purchase_success',
            val => {
              if (val === "true") {
                getProfile(false);
              }
            },
          );
          return () => eventEmitter;
    }, [userInfo]);

    const getProfile = async (refresh) => {
        if (userInfo?.access_token) {
            if (!refresh) {
                setLoading(true);
            }
            const lanData = {
                language : lang
            }
            let encryptLanData = encryptData(JSON.stringify(lanData), userInfo.secret_key);
            const response = await fetchPostByToken(apiUrl.profile, {data: encryptLanData}, userInfo.access_token);

            if (response?.success && response?.data) {
                const decUserInfo = JSON.parse(decryptData(response.data, userInfo.secret_key));

                setProfileData(decUserInfo.user);
                changeProfileInfo(decUserInfo.user);
            }
            else if (response?.status === 401) {
                logoutHandler();
                setExpired(true);
            }
            setLoading(false);
            setRefreshing(false);
        } else {
            setLoading(false);
            setRefreshing(false);
        }
    }

    const handleCancel = () => {}

    const showConfirmAlert = () => {
        Alert.alert(
            local.confirmation,
            local.confirmLogout,
          [
            { text: local.cancel, onPress: handleCancel, style: 'cancel' },
            { text: local.confirm, onPress: logoutHandler },
          ],
          { cancelable: false } // Prevent dismissing the alert by tapping outside or pressing the back button
        );
      };

    const logoutHandler = async () => {
        await deleteMultiItems(['@userInfo']);
        changeUserInfo(null);
        changeProfileInfo();
        setProfileData();
    }

    const pressHandler = (id) => {
        if (id === 12) {
            navigation.navigate('ChangePassword');
        } else  if (id === 13) {
            setShowLan(true);
        } else if (id === 14) {
            navigation.navigate("Recharge");
        } else if (id === 15) {
            navigation.navigate("Withdraw");
        } else if (id === 22) {
            navigation.navigate('ContactUs', {agent_phone : profileData?.agent_phone})
        } else if (id === 31) {
            navigation.navigate('FAQ')
        } else if (id === 32) {
            navigation.navigate('Feedback')
        } else if (id === 21) {
            navigation.navigate('AboutUs')
        } else if (id === 23) {
            Linking.openURL("http://play.google.com/store/apps/details?id=com.facebook.katana")
        } else if (id === 24) {
            setShowTerms(true);
        } else if (id === 30) {
            if (profileInfo?.agent_phone) {
                Linking.openURL(`tel:${profileInfo.agent_phone}`)
            } else {
                ToastAndroid.show(local.noAgentPhone, ToastAndroid.SHORT);
            }
        } else if (id === 50) {
            Linking.openURL("https://goldencastlelottery.com/pravicy-policy")
        } else if (id === 33) {
            navigation.navigate('AccountDeletion')
        } 
    }

    const closeTerms = () => {
        setShowTerms(false);
    }

    const openLoginModal = () => {
        setShowLogin(true);
        setDefaultTab(1);
    }

    const editNameHandler =() => {
        navigation.navigate("EditProfile", {profileData : profileData ? profileData : null});
    }
    
    const lanHandler = async (lan) => {
        setShowLan(false);
        setLoading(true);
        changeLang(lan);
        setItem('@lang', lan);

        if (userInfo && userInfo.access_token && lang !== lan) {
            let encryptLanData = encryptData(JSON.stringify({language: lan}), userInfo.secret_key);
            await fetchPostByToken(apiUrl.updateLanguage, {data: encryptLanData}, userInfo.access_token);
        }

        setLoading(false);
    }

    if (showTerms) return <View style={styles.modalContainer}>
        <Terms onClose={ closeTerms } />
    </View>

    return ( 
        <ScrollView
            style={styles.container}
            showsVerticalScrollIndicator={false}
            refreshControl={
                <RefreshControl
                  refreshing={ refreshing }
                  colors={[ "#ff9700" ]}
                  onRefresh={ onRefresh }
                  progressViewOffset={hp(4)} />
              }>
            <StatusBar barStyle={'light-content'} backgroundColor="#11212f" />
            <View style={styles.header}>
                <View style={[styles.infoContent, {
                    justifyContent: userInfo ? 'space-between' : 'center'
                }]}>
                    {
                        userInfo ? 
                            <View style={styles.infoContent2}>
                                <FontAwesome
                                    name="user-circle-o"
                                    size={hp(4)}
                                    color='#ff9700'
                                    style={styles.profile}  />
                                <View>
                                    <View style={ styles.nameRow }>
                                        <Text style={styles.name}>
                                            { profileData ? profileData.name : ''}
                                        </Text>
                                        {
                                            profileData?.name && (
                                                <TouchableOpacity
                                                    activeOpacity={0.8}
                                                    style={ styles.editBtn }
                                                    disabled={!profileData}
                                                    onPress={ editNameHandler }>
                                                    <Feather
                                                        name="edit-3"
                                                        size={wp(3)}
                                                        color='#ff9700' />
                                                </TouchableOpacity>
                                            )
                                        }
                                    </View>
                                    <Text style={styles.username}>
                                        {`@${profileData ? profileData.user_name : '-'}`}
                                    </Text>
                                </View>
                            </View> :
                            <View style={styles.infoContent3}>
                                <Image 
                                    source={require('../../assets/images/logo_only.png')}
                                    style={styles.logoProfile}/>
                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    style={styles.loginBtn}
                                    onPress={ openLoginModal }>
                                    <Text style={styles.loginTxt}>
                                        {local.login}
                                    </Text>
                                </TouchableOpacity>
                            </View>

                    }
                    {
                        userInfo && (
                            <View style={ styles.pointContent }>
                                <TouchableOpacity 
                                    activeOpacity={1}
                                    style={styles.agentContent}>
                                    <Text style={styles.agent}>
                                        {local.agent}:&nbsp;
                                    </Text>
                                    <Text style={styles.agentCode}>
                                        { profileData ? profileData.agent_code : '' }
                                    </Text>
                                </TouchableOpacity>
                                <View style={ styles.pointRow }>
                                    <FontAwesome5
                                        name="coins"
                                        size={hp(1.6)}
                                        color='#ff9700' />
                                    <Text style={styles.point}>
                                        {profileData ? profileData.point : '0'}
                                    </Text>
                                </View> 
                            </View>
                        )
                        
                    }
                </View>
               
            </View>
            
            <View style={styles.middleContent}>
                <TouchableOpacity 
                    style={styles.purchaseContent} 
                    disabled={!userInfo}
                    onPress={() =>  navigation.navigate('PurchasedTickets') }>
                    <View>
                        <Text style={styles.ticket}>
                           {local.purchasedTickets}
                        </Text>
                    </View>
                    <View style={styles.detailBtn}>
                        <Text style={styles.detailTxt}>
                            {
                                profileData ? `${profileData.current_ticket_count} ${local.notickets}` : `0 ${local.notickets}`
                            }
                        </Text>
                        <Entypo
                            name="chevron-small-right"
                            size={hp(1.7)}
                            color="#fff" />
                    </View>
                </TouchableOpacity>
                <View style={styles.lotteryRow}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={styles.lotteryBtn}
                        onPress={()=> navigation.navigate('WinnerList')}>
                        <View style={styles.icon}>
                            <Crown />
                        </View>
                        <Text style={styles.lotteryTitle}>
                            {local.winnerList}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={styles.lotteryBtn}
                        disabled={!profileData}
                        onPress={()=> Linking.openURL(profileData?.live_opening)}>
                            
                        <View style={styles.icon}>
                            <Youtube />
                        </View>
                        <Text style={styles.lotteryTitle}>
                           {local.liveOpening}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View> 
            <View>
               
                {
                    userInfo ? SETTINGS_MENUS.map((menu, index) => {
                        return <View style={[styles.settings, {
                            marginTop: index === 0 ? hp(0) : hp(2)
                        }]}>
                            <Text style={styles.settingTitle}>
                                { menu.title }
                            </Text>
                            <View style={styles.settingBlock}>
                                {
                                    menu.sub_menus.map((submenu, index) => {
                                        return <ProfileMenu
                                            key={ index }
                                            pressHandler={()=> pressHandler(submenu.id)}
                                            icon={ submenu.icon }
                                            menuTitle={ submenu.title }
                                            latest={ index === menu.sub_menus.length - 1 } />
                                    })
                                }
                            </View>
                        </View>
                    }) : 
                    SETTINGS_MENUS_UNKNOWN.map((menu, index) => {
                        return <View style={[styles.settings, {
                            marginTop: index === 0 ? hp(0) : hp(2),
                            marginBottom: index === SETTINGS_MENUS_UNKNOWN.length - 1 ? hp(16) : 0
                        }]}>
                            <Text style={styles.settingTitle}>
                                { menu.title }
                            </Text>
                            <View style={styles.settingBlock}>
                                {
                                    menu.sub_menus.map((submenu, index) => {
                                        return <ProfileMenu
                                            pressHandler={()=> pressHandler(submenu.id)}
                                            icon={ submenu.icon }
                                            menuTitle={ submenu.title }
                                            latest={ index === menu.sub_menus.length - 1 } />
                                    })
                                }
                            </View>
                        </View>
                    })
                }
                {
                    userInfo && <TouchableOpacity
                        activeOpacity={0.8}
                        style={styles.logoutBtn}
                        onPress={ showConfirmAlert }>
                        <AntDesign 
                            name="logout"
                            size={hp(2)}
                            color='#ff9700' />
                        <Text style={styles.logout}>
                            {local.logout}
                        </Text>
                    </TouchableOpacity>
                }
                {
                    showLan && <Language 
                        closeModalHandler={()=> setShowLan(false)}
                        lanHandler={ lanHandler } />
                }
            </View>
            {
                showLogin && (
                    <LoginModal
                        onClose={()=> setShowLogin(false)}
                        defaultTab={ defaultTab } />
                )
            }
            {
                expired && <SessionExpired
                    closeModalHandler={()=>  {
                        setExpired(false);
                    }}/>
            }
        </ScrollView>
    )
}

export default Profile;