import React, {
    useState,
    useContext,
    useEffect
} from 'react';
import {
    View,
    Text,
    ScrollView
} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import Ionicons from 'react-native-vector-icons/Ionicons';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

import { fetchPost } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { Context } from '../../context/Provider';
import { formatNumber } from '../../utils/common';
import { useLocal } from '../../hook/useLocal';
import { encryptData } from '../../utils/encryptData';
import { decryptData } from '../../utils/decryptData';

import styles from './Style';
import Header from '../../components/header/DetailHeader';
import ResultDateModal from "../../components/modal/ResultDateModal";
import Prize from '../../assets/icons/Prize';
import Loading from '../../components/loading/Loading';
import NoData from '../../components/noData/NoData';
import NetworkProblem from '../../components/modal/NetworkProblem';

const WinnerList = ({navigation}) => {

    const { 
        lang,
        net
    } = useContext(Context);

    const local = useLocal();

    const lanData = {
        language: lang
    }
    const encryptLanData = encryptData(JSON.stringify(lanData));

    const [
        drawDates,
        setDrawDates
    ] = useState([]);
    const [
        selectedDate,
        setSelectedDate
    ] = useState('');
    const [
        showModal,
        setShowModal
    ] = useState(false);
    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        allWinners,
        setAllWinners
    ] = useState([]);
    const [
        noData,
        setNoData
    ] = useState(false);
    const [
        winners,
        setWinners
    ] = useState([]);

    useEffect(() => {
        if (net) getDrawDates();
    }, [net]);

    const getDrawDates = async () => {
        setLoading(true);
        const response = await fetchPost(apiUrl.getDrawDates, {data: encryptLanData});

        if (response?.data?.data) {
            const decDrawDates = JSON.parse(decryptData(response.data.data));
            if (decDrawDates?.draw_ids) { 
                setSelectedDate(decDrawDates.draw_ids[0]);
                setDrawDates(decDrawDates.draw_ids);
                getWinnerList(decDrawDates.draw_ids[0]);
            } 
        } else {
            setLoading(false);
            setNoData(true);
        }
    }

    const getWinnerList = async (date) => {
        const response = await fetchPost(apiUrl.winnerList, {data: encryptLanData});
        if (response?.success && response?.data?.data) {
            const decWinners = JSON.parse(decryptData(response.data.data));
            setAllWinners(decWinners?.results);
            if (decWinners?.results[date]) {
                setWinners(decWinners.results[date]);
                if (decWinners.results[date]?.length > 0) {
                    setNoData(false);
                } else {
                    setNoData(true);
                }
            } else {
                setNoData(true)
            }
        } else {
            setNoData(true);
        }
        setLoading(false);
    }

    const dateHandler = val => {
        setShowModal(false);
        setSelectedDate(val);
        if (allWinners[val]) {
            setWinners(allWinners[val]);
            if (allWinners[val]?.length > 0) {
                setNoData(false);
            } else {
                setNoData(true);
            }
        } else {
            setWinners(null);
            setNoData(true);
        }

    }

    const WinnersCard = ({title, results}) => {
        return (
            <View style={[styles.card, {marginTop: hp(6)}]}>
                <View style={styles.titleContent}>
                    <View style={styles.crown}>
                        <FontAwesome5
                            name="crown"
                            size={hp(2.3)}
                            color='#ff9700' />
                    </View>
                    <Text style={styles.title}>
                        { title }
                    </Text>
                </View>

                {
                    results.map((result, index) => {
                        return <View style={[styles.winnerContent, {marginTop: hp(2.5)}]}>
                            <Text style={styles.order}>
                                { index + 1 }
                            </Text>
                            <Ionicons 
                                name="md-person-circle-outline"
                                size={wp(12)}
                                color='#ababa7' />
                            <View style={styles.nameContent}>
                                <Text style={styles.name}>
                                    { result.name }
                                </Text>
                                <View style={styles.row}>
                                    <Prize />
                                    <Text style={styles.prizeAmt}>
                                        {`${formatNumber(result.amount)} ${"\u0E3F"}`}
                                    </Text>
                                </View> 
                            </View>
                            <View style={styles.ticketnoContent}>
                                <View style={styles.ticketBg}>
                                    <Text style={styles.ticketno2}>
                                        {result.ticket_number.charAt(0)}
                                    </Text>
                                </View>
                                <View style={styles.ticketBg}>
                                    <Text style={styles.ticketno2}>
                                        {result.ticket_number.charAt(1)}
                                    </Text>
                                </View>
                                <View style={styles.ticketBg}>
                                    <Text style={styles.ticketno2}>
                                    {result.ticket_number.charAt(2)}
                                    </Text>
                                </View>
                                <View style={styles.ticketBg}>
                                    <Text style={styles.ticketno2}>
                                    {result.ticket_number.charAt(3)}
                                    </Text>
                                </View>
                                <View style={styles.ticketBg}>
                                    <Text style={styles.ticketno2}>
                                    {result.ticket_number.charAt(4)}
                                    </Text>
                                </View>
                                <View style={styles.ticketBg}>
                                    <Text style={styles.ticketno2}>
                                    {result.ticket_number.charAt(5)}
                                    </Text>
                                </View>
                            </View>
                        </View>
                    })
                }
            
            </View>
        )
    }

    return (
        <View style={styles.container}>
 
            <Header
                title={local.winnerList}
                goBack={()=> navigation.goBack()}
                showDate={()=> setShowModal(true)}
                date={ selectedDate } />
            {
                !net ?
                    <NetworkProblem /> :
                    <>
                        {!loading && winners && (
                            <ScrollView showsVerticalScrollIndicator={false} style={{marginTop: hp(0), marginBottom: hp(10)}}>
                                {winners.first && <WinnersCard title={local.firstPrizeWinners} results={winners.first} />}
                                {winners.second && <WinnersCard title={local.secondPrizeWinners} results={winners.second} />}
                                {winners.third && <WinnersCard title={local.thirdPrizeWinners} results={winners.third} />}
                                {winners.forth && <WinnersCard title={local.fourthPrizeWinners} results={winners.forth} />}
                                {winners.fifth && <WinnersCard title={local.fifthPrizeWinners} results={winners.fifth} />}
                                {winners.first_three && <WinnersCard title={local.first3DigitsWinners} results={winners.first_three} />}
                                {winners.last_three && <WinnersCard title={local.last3DigitsWinners} results={winners.last_three} />}
                                {winners.last_two && <WinnersCard title={local.last2DigitsWinners} results={winners.last_two} />}
                            </ScrollView>
                        ) }
                            
                        {loading && <Loading />}

                        {
                            !loading && noData && <View  style={styles.nodata}>
                                <NoData message={local.noWinnerList} />
                            </View>
                        }

                        {showModal && <ResultDateModal
                            drawDates={ drawDates }
                            closeModalHandler={()=> setShowModal(false)}
                            dateHandler={(val) => dateHandler(val)} />}
                    </>
            }
            
        </View>
    )
}

export default WinnerList;