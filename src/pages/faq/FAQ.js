import React, {
    useState,
    useEffect,
    useContext
} from 'react';
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Entypo from "react-native-vector-icons/Entypo";
import AntDesign from 'react-native-vector-icons/AntDesign'

import { Context } from '../../context/Provider';
import { fetchPost } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { useLocal } from '../../hook/useLocal';
import { encryptData } from '../../utils/encryptData';
import { decryptData } from '../../utils/decryptData';

import styles from './Style';
import Header from '../../components/header/DetailHeader';
import Loading from '../../components/loading/Loading';
import NoData from '../../components/noData/NoData';
import NetworkProblem from '../../components/modal/NetworkProblem';

const FAQ = ({navigation}) => {

    const local = useLocal();
    const { 
        net,
        lang
    } = useContext(Context);

    const lanData = {
        language : lang
    }
    let encryptLanData = encryptData(JSON.stringify(lanData));

    const [
        active,
        setActive
    ] = useState();
    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        faqs,
        setFaqs
    ] = useState();
    const [
        noData,
        setNoData
    ] = useState(false);

    useEffect(() => {
        if (net) getFAQ();
    }, [net]);

    const getFAQ = async () => {
        setLoading(true);
        const response = await fetchPost(apiUrl.faqs, {data: encryptLanData});
           
        if (response?.success && response?.data?.data) {
            const decFaq = JSON.parse(decryptData(response.data.data));
            if (decFaq?.faqs?.length > 0) {
                setFaqs(decFaq.faqs);
            } else {
                setFaqs([]);
                setNoData(true);
            }

        } else {
            setNoData(true);
        }
        setLoading(false);
    }

    const onClose = () => {
        navigation.goBack();
    }

    return (
        <View style={styles.container}>
            <Header
                title={local.frequentlyAskedQues}
                goBack={onClose}/>
            {
                !net ?
                    <NetworkProblem /> :
                    <>
                        {
                            !loading && faqs && (
                                <ScrollView showsVerticalScrollIndicator={false}>
                            {faqs?.length > 0 && <Text style={styles.faq}>
                                {local.faqDescrition}
                            </Text>}
                            {
                                faqs.map((faq, index) => {
                                    return (
                                        <View style={styles.card}>
                                            <TouchableOpacity
                                                activeOpacity={0.8}
                                                style={[styles.toggleContent, {
                                                    paddingBottom: active === index ? 0 : hp(2)
                                                }]}
                                                onPress={()=> {
                                                    if (index === active) {
                                                        setActive()
                                                    } else {
                                                        setActive(index)
                                                    }
                                                }}>
                                                <View style={styles.left}>
                                                    <AntDesign 
                                                        name="questioncircle"
                                                        size={hp(2)}
                                                        color='#ff9700' />
                                                    <Text style={styles.title}>
                                                        {faq.question}
                                                    </Text>
                                                </View>
                                                <Entypo 
                                                    name={active === index ? 'chevron-small-up' : 'chevron-small-down'}
                                                    size={hp(2)}
                                                    color='gray' />
                                            </TouchableOpacity>
                                            {
                                                active === index && (
                                                    <View style={styles.descContent}>
                                                        <Text style={styles.description}>
                                                            {faq.answer}
                                                        </Text>
                                                    </View>
                                                )
                                            }
                                        </View>
                                    )
                                })
                            }
                        </ScrollView>
                            )
                        }
                        {
                            !loading && noData && <View style={{
                                height: hp(80)   
                            }}>
                                <NoData message={local.noData} />
                            </View>
                        }
                        {loading && <Loading />}
                    </>
            }
        </View>
    )
}

export default FAQ;