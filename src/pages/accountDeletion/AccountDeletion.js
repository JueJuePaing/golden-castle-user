import React, {
    useState,
    useEffect,
    useContext
} from 'react';
import {
    View,
    TextInput,
    Text,
    ToastAndroid,
    TouchableOpacity
} from 'react-native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Fontisto from 'react-native-vector-icons/Fontisto'

import { Context } from '../../context/Provider';
import { fetchPost } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { useLocal } from '../../hook/useLocal';
import { encryptData } from '../../utils/encryptData';
import { decryptData } from '../../utils/decryptData';

import { fetchPostByToken } from '../../utils/fetchData';


import styles from './Style';
import Header from '../../components/header/DetailHeader';
import Loading from '../../components/loading/Loading';
import NetworkProblem from '../../components/modal/NetworkProblem';

const AccountDeletion = ({navigation}) => {

    const local = useLocal();
    const { 
        net,
        lang,
        userInfo
    } = useContext(Context);


    const REASONS = [
        {
            id: 1,
            reason : local.privacyConcerns
        },
        {
            id: 2,
            reason : local.personalReasons
        },
        {
            id: 3,
            reason : local.changeInInterests
        },
        {
            id: 6,
            reason : local.others
        },
    ]

    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        selectedReason,
        setSelectedReason
    ] = useState();
    const [
        reasonText,
        setReasonText
    ] = useState();


    const onClose = () => {
        navigation.goBack();
    }

    const deleteHandler = async () => {

        if (!net) {
            ToastAndroid.show(local.noConnection, ToastAndroid.SHORT);
            return;
        }
       
        setLoading(true);

        if (userInfo && userInfo.access_token) {
            const data = {
               reason: selectedReason ?
                selectedReason.id === 6 ? reasonText :
               selectedReason.reason : ''
            }
            let encryptFeebackData = encryptData(JSON.stringify(data), userInfo.secret_key);

            const response = await fetchPostByToken(apiUrl.userAccountDelete, {data: encryptFeebackData}, userInfo.access_token);


            if (response?.success) {
                ToastAndroid.show(local.deleteRequestSuccessful, ToastAndroid.SHORT);
                navigation.goBack();
            } else {
                ToastAndroid.show(local.somethingWrong, ToastAndroid.SHORT);        
            }

        } else {
            ToastAndroid.show(local.unauthenticated, ToastAndroid.SHORT);
        }
        setLoading(false);
    }

    return (
        <View style={styles.container}>
            <Header
                title={local.deleteAccountRequest}
                goBack={onClose}/>
            {
                !net ?
                    <NetworkProblem /> :
                    <View>
                        <Text style={styles.chooseTitle}>
                            {local.chooseReason}
                        </Text>
                        {REASONS.map((reason) => {
                            return <TouchableOpacity
                                activeOpacity={.7}
                                key={reason.id}
                                onPress={()=> setSelectedReason(reason)}
                                style={styles.row}>
                                <Fontisto
                                    name={selectedReason && selectedReason.id === reason.id ? 'radio-btn-active' : 'radio-btn-passive'}
                                    size={hp(2.3)}
                                    color="#ff9700" />
                                <Text style={styles.reason}>{reason.reason}</Text>
                            </TouchableOpacity>
                        })}

                        {selectedReason && selectedReason.id === 6 && <TextInput
                            // showSoftInputOnFocus={false}
                            value={reasonText}
                            multiline={true}
                            numberOfLines={4}
                            textAlignVertical="top"
                            placeholder={local.enterReason}
                            onChangeText={setReasonText}
                            style={styles.inputText}/>

}
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={styles.deleteBtn}
                            onPress={ deleteHandler }>
                            <Text style={styles.delete}>
                                {local.requestDelete}
                            </Text>
                        </TouchableOpacity>
                    </View>
            }
            {loading && <Loading />}
            
        </View>
    )
}

export default AccountDeletion;