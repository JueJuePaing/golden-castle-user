import { StyleSheet, StatusBar } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    deleteBtn: {
        alignSelf: 'center',
        marginVertical: hp(2),
        paddingHorizontal: wp(4),
        // paddingVertical: hp(.7),
        flexDirection: 'row',
        alignItems: 'center',
       
        borderRadius: hp(1),
        backgroundColor: '#cf463a',
        shadowOffset: {width: 5, height: 10},
        shadowColor: '#000',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3,

        marginBottom: hp(16),
        height: hp(5)
    },
    delete: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.7),
        color: '#fff',
        paddingTop: hp(.5),
        paddingLeft: wp(1)
    },
    chooseTitle: {
        paddingVertical: hp(2),
        paddingHorizontal: wp(3),
        backgroundColor: '#fff',
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.7),
        color: 'gray',
        textAlign: 'center',
        marginBottom: hp(1)
    },
    row: {
        paddingHorizontal: wp(5),
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom : hp(3)
    },
    reason: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.9),
        color: '#000',
        paddingLeft: wp(4)
    },
    inputText: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.7),
        color: '#2d3842',
        width: wp(90),
        alignItems: 'center',
        paddingVertical: hp(1.5),
        borderRadius: hp(0.5),
        paddingHorizontal: hp(1),
        borderColor: '#fff',
        backgroundColor: '#F2F2F2',
        alignSelf: 'center',
        paddingHorizontal: wp(2)
    }
})

export default styles;