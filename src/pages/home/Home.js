import React, {
    useState,
    useContext,
    useEffect,
    useCallback,
    useRef,
    useMemo
} from 'react';
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Linking,
    StatusBar,
    Modal,
    Image,
    ToastAndroid,
    RefreshControl
} from 'react-native';
import styles from './Style';
import {heightPercentageToDP as hp,   widthPercentageToDP as wp} from 'react-native-responsive-screen';
import { Context } from '../../context/Provider';
import moment from 'moment';
import {useFocusEffect} from "@react-navigation/native";

import { fetchPost, fetchPostByToken } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { useLocal } from '../../hook/useLocal';
import { setItem } from '../../utils/appStorage';
import { formatNumber } from '../../utils/common';
import { encryptData } from '../../utils/encryptData';
import { decryptData } from '../../utils/decryptData';

import Ticket from '../../components/ticket/Ticket';
import SlideImageHeader from '../../components/slideImage/slideImageHeader';
import Header from "../../components/header/HomeHeader";
import Countdown from '../../components/countdown/Countdown';
import TicketHeader from '../../components/ticketHeader/TicketHeader';
import Language from '../../components/language/Language';
import LoginModal from '../auth/Login';
import Loading from '../../components/loading/Loading';
import BuyTicket from '../buyTicket/BuyTicket';
import NetworkProblem from '../../components/modal/NetworkProblem';
import ForceUpdate from '../../components/modal/ForceUpdate';
import TicketNumber from '../../components/ticketNumber/TicketNumber';

const Home = ({navigation}) => {

    const local = useLocal();

    const { 
        changeLang,
        userInfo,
        lang,
        net,
        profileInfo,
        bannersData,
        countdownData,
        liveSaleData,
        lastDrawData,
        changeProfileInfo,
        changeCountdownData,
        changeBanners,
        changeLiveSaleData,
        changeLastDrawData
    } = useContext(Context);

    const lanData = {
        language : lang
    }
    let encryptLanData = encryptData(JSON.stringify(lanData));

    const [
        refreshing,
        setRefreshing
      ] = useState(false);
    const [
        showLan,
        setShowLan
    ] = useState(false);
    const [
        showLogin,
        setShowLogin
    ] = useState(false);
    const [
        defaultTab,
        setDefaultTab
    ] = useState(1);
    const [
        showBuy,
        setShowBuy
    ] = useState(false);
    const [
        defaultTicket,
        setDefaultTicket
    ] = useState();
    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        banners,
        setBanners
    ] = useState([]);
    const [
        newBanners,
        setNewBanners
    ] = useState([]);
    // const [
    //     bannerWithBiggestHeight,
    //     setBannerWithBiggestHeight
    // ] = useState();
    const [
        countdownDate,
        setCountdownDate
    ] = useState(null);
    const [
        drawDate,
        setDrawDate
    ] = useState();
    const [
        countdownDay,
        setCountdownDay
    ] = useState();
    const [
        countdownHour,
        setCountdownHour
    ] = useState();
    const [
        countdownMin,
        setCountdownMin
    ] = useState();
    const [
        countdownSec,
        setCountdownSec
    ] = useState();
    const [
        liveTickets,
        setLiveTickets
    ] = useState();
    const [
        lastDrawTickets,
        setLastDrawTickets
    ] = useState();
    const [
        forceUpdate,
        setForceUpdate
    ] = useState(false);
    const [
        showCongratulation,
        setShowCongratulation
      ] = useState(false);
      const [
        winning_tickets,
        setWinningTickets
      ] = useState([]);
      const [
        contactInfo,
        setContactInfo
    ] = useState();

    const isPageLoaded = useRef(false);
    const [isLoading, setIsLoading] = useState(true);

    const targetWidth = wp(92);

    useEffect(() => {

        let isMounted = true; 

        const getImageHeight = async () => {
          const newData = await Promise.all(banners.map(async (item) => {
            return new Promise((resolve, reject) => {
              Image.getSize(item, (originalWidth, originalHeight) => {

                if (!isMounted) {
                    return; // Component is unmounted, don't update state
                  }

                const aspectRatio = originalWidth / originalHeight;
                const targetHeight = targetWidth / aspectRatio;

                const returnData = {
                  height: Math.floor(targetHeight),
                  url: item,
                };
                resolve(returnData);
              }, (error) => {

                if (!isMounted) {
                    return; // Component is unmounted, don't update state
                  }

                console.error('Error getting image size:', error);
                const returnData = {
                  height, // Set a default height
                  url: item,
                };
                resolve(returnData); // Resolve with the default data
              });
            });
          }));
    
           if (isMounted) {
            setNewBanners(newData);
           }
        }
        if (banners && banners.length > 0) {
          getImageHeight();
        }

        return () => {
            isMounted = false; // Set the flag to indicate unmounting
            // You can also cancel any ongoing async operations here if needed
          };
      }, [banners]);

      const biggestBanner = useMemo(() => {
        if (newBanners?.length > 0) {
            const biggestItem = newBanners?.reduce((prev, current) => {
                return prev.height > current.height ? prev : current;
            }, newBanners);
            return biggestItem;
        } else {
            return null;
        }
      }, [newBanners]);

    useEffect(() => {
        // Simulating an asynchronous operation
        setTimeout(() => {
          // Set the flag to indicate that the page has finished loading
          isPageLoaded.current = true;
          setIsLoading(false);
        }, 2000);
      }, []);

    const getContactData = async () => {
        const response = await fetchPost(apiUrl.contactInfo, {data: encryptLanData});
        
        if (response?.success && response?.data?.data) {
            const decContactData = JSON.parse(decryptData(response.data.data));
            setContactInfo(decContactData);
        }
    }

    function convertMS(ms) {
        var d, h, m, s;
        s = Math.floor(ms / 1000);
        m = Math.floor(s / 60);
        s = s % 60;
        h = Math.floor(m / 60);
        m = m % 60;
        d = Math.floor(h / 24);
        h = h % 24;
        h += d * 24;
        return {h, m, s}
    }

    useEffect(() => {
        const interval = setInterval(() => {
            if (countdownDay) {
                const start = new Date();
                const end = new Date();
                end.setDate(end.getDate() + 1);
                end.setHours(0,0,0,0)
        
                const diffTime = Math.abs(end - start);
                const result = convertMS(diffTime);
                setCountdownHour(result.h);
                setCountdownMin(result.m);
                setCountdownSec(result.s);
            }
        }, 1000);
        return () => {
            if(interval) clearInterval(interval);
        };
    }, [countdownDay]);

    useEffect(() => {
        if (!showBuy) {
            setLoading(true);
            getContactData();
            getInitialData(false);
        }
    }, [showBuy]);

    useEffect(() => {
        if (bannersData) {
            setBanners(bannersData);
        }
        if (countdownData) {
            if (countdownData?.current_draw) {
                setCountdownDate([moment(countdownData.current_draw).format('D'), moment(countdownData.current_draw).format('MMM')]);
                setDrawDate(moment(countdownData.current_draw).format('D MMMM YYYY'))
                setItem('@nextdraw', countdownData.current_draw);
                setItem('@nextdrawDate', moment(countdownData.current_draw).format('D MMMM YYYY'));
            }
            if (countdownData?.day_diff) {
                setCountdownDay(countdownData.day_diff);
            }
        }
        if (liveSaleData) {
            setLiveTickets(liveSaleData);
        }
        if (lastDrawData) {
            setLastDrawTickets(lastDrawData);
        }
    }, [bannersData, countdownData, liveSaleData, lastDrawData])

    const getInitialData = async (refresh) => {
            const response = await fetchPost(apiUrl.banners, {data: encryptLanData});

            if (response?.success && response?.data?.data) {
                const bannerData = JSON.parse(decryptData(response.data.data));
                setBanners(bannerData?.banner_urls);
                // changeBanners(bannerData?.banner_urls)
                await setItem('@offline_banners', JSON.stringify(bannerData?.banner_urls));
                await setItem('@exchange_rate', bannerData?.exchange_rate + '');
            } else {
                if (response?.status === 429) {
                    ToastAndroid.show(local.tooManyAttempts, ToastAndroid.SHORT);
                } 
            }

            const countdownResponse = await fetchPost(apiUrl.countdown, {data: encryptLanData});
            if (countdownResponse?.success && countdownResponse?.data?.data) {
                const decCountDown = JSON.parse(decryptData(countdownResponse.data.data));
                await setItem('@offline_countdown', JSON.stringify(decCountDown));
                if (decCountDown?.current_draw) {
                    setCountdownDate([moment(decCountDown.current_draw).format('D'), moment(decCountDown.current_draw).format('MMM')]);
                    setDrawDate(moment(decCountDown.current_draw).format('D MMMM YYYY'))
                    setItem('@nextdraw', decCountDown.current_draw);
                    setItem('@nextdrawDate', moment(decCountDown.current_draw).format('D MMMM YYYY'));
                }
                if (decCountDown?.day_diff) {
                    setCountdownDay(decCountDown.day_diff);
                }
            }
            const liveTicketsResponse = await fetchPost(apiUrl.liveTickets, {data: encryptLanData});
            
            if (liveTicketsResponse?.success && liveTicketsResponse?.data?.data) {
                const decLive= JSON.parse(decryptData(liveTicketsResponse.data.data));
                setLiveTickets(decLive);
                await setItem('@offline_liveTickets', JSON.stringify(decLive));
            }

            const lastDrawResponse = await fetchPost(apiUrl.lastDrawTickets, {data: encryptLanData});
            if (lastDrawResponse?.success && lastDrawResponse?.data?.data) {
                const decLast = JSON.parse(decryptData(lastDrawResponse.data.data));
                setLastDrawTickets(decLast);
                await setItem('@offline_lastDraw', JSON.stringify(decLast));
            }

        setRefreshing(false);
        setTimeout(() => {
            setLoading(false);
        }, 3000);
    }

    const getProfileData = async () => {
        let encryptLanData = encryptData(JSON.stringify(lanData), userInfo?.secret_key);

        const response = await fetchPostByToken(apiUrl.profile, {data: encryptLanData}, userInfo.access_token);
        if (response?.success && response?.data) {
            const decUserInfo = JSON.parse(decryptData(response.data, userInfo.secret_key));
            changeProfileInfo(decUserInfo.user);
        }
    }

    const onRefresh = useCallback(() => {
    
        const reloadData = async () => {
            setRefreshing(true);
            getContactData();
            getProfileData();
            getInitialData(true);
        };
    
        reloadData();
      }, []);

    useFocusEffect(
        useCallback(() => {
            
            checkLotteryWin(userInfo);
        }, [userInfo]),
    );

    const checkLotteryWin = async (info) => {
        if (info?.access_token) {
            let encryptLanData = encryptData(JSON.stringify(lanData), info.secret_key);
            const response = await fetchPostByToken(apiUrl.checkLotteryWon, {data: encryptLanData}, info.access_token);
  
          if (response?.success && response?.data) {
            const decLotteryWin = JSON.parse(decryptData(response.data, info.secret_key));
            if (decLotteryWin?.results?.length > 0) {
              setWinningTickets(decLotteryWin.results);
              setShowCongratulation(true);
            }
          }
        }
      }

      const onSubmit = async () => {
        setShowCongratulation(false);
        if (userInfo?.access_token) {
            let encryptLanData = encryptData(JSON.stringify(lanData), userInfo.secret_key);
            await fetchPostByToken(apiUrl.readLotteryWon, {data: encryptLanData}, userInfo.access_token);
        }
      }

    const openLoginModal = () => {
        setShowLogin(true);
        setDefaultTab(1);
    }

    const openRegisterModal = () => {
        setShowLogin(true);
        setDefaultTab(2);
    }

    const lanHandler = async (lan) => {
        setShowLan(false);
        setLoading(true);
        changeLang(lan);
        setItem('@lang', lan);

        if (userInfo?.access_token && lang !== lan) {
            let encryptLanData = encryptData(JSON.stringify({language: lan}), userInfo.secret_key);
            await fetchPostByToken(apiUrl.updateLanguage, {data: encryptLanData}, userInfo.access_token);
        }
        setLoading(false);
    }

    const reloadHandler = () => {
        if (net) {
            setLoading(true);
            getInitialData(false);
        }
    }

    if (!net) {
        return <NetworkProblem reloadHandler={reloadHandler} />
    }

    const updateHandler = () => {
        Linking.openURL("http://play.google.com/store/apps/details?id=com.facebook.katana")
    }

    if (forceUpdate) {
        return <ForceUpdate updateHandler={updateHandler} />
    }


  else if (showCongratulation)
  return (
    <Modal
            transparent={true}
            visible={true}
            animationType="fade">
            <Image
                source={require('../../assets/images/congratulation.jpg')}
                style={styles.conImg}/>
            <View style={styles.modalView}>

                <View style={styles.modalBg}>
                    <Image
                        source={require('../../assets/images/crown.png')}
                        style={styles.crown}/>

                    <ScrollView showsVerticalScrollIndicator={false}>
                        {
                            winning_tickets.map((winner, index) => {
                                return (
                                    <View
                                        key={index}
                                        style={styles.prizeRow}>
                                        <Text style={styles.prizeTitle}>
                                         {
                                                winner.type === "first" ? local.firstPrize :
                                                winner.type === "closest_first" ? local.closetFirstPrize :
                                                winner.type === "second" ? local.secondPrize :
                                                winner.type === "third" ? local.thirdPrize :
                                                winner.type === "forth" ? local.fourthPrize :
                                                winner.type === "fifth" ? local.fifthPrize : 
                                                winner.type === "first_three" ? local.first3Digits :
                                                winner.type === "last_three" ? local.last3Digits :
                                                winner.type === "last_two" ? local.last2Digits : ""
                                            }
                                        </Text>
                                        <Text style={styles.prizeAmt}>
                                            {`${formatNumber(winner.amount)} ${"\u0E3F"}`}
                                        </Text>
                                        {
                                            winner.tickets.map((ticket, index) => {
                                                return <TicketNumber key={index} number={ticket.toString()} color="#ff9700" />
                                            })
                                        }
                                    </View>
                                )
                            })
                        }
                    </ScrollView>
                </View>
                <TouchableOpacity 
                    activeOpacity={0.8}
                    onPress={onSubmit}
                    style={styles.submitBtn}>
                    <Text style={styles.submit}>{local.ok}</Text>
                </TouchableOpacity>
            </View>
        </Modal>
  )

//  if (isLoading) return <Loading />

    return (
        <ScrollView 
            style={styles.container} 
            showsVerticalScrollIndicator={false}
            refreshControl={
                <RefreshControl
                    refreshing={ refreshing }
                    colors={[ "#ff9700" ]}
                    onRefresh={ onRefresh }
                    progressViewOffset={hp(4)} />
              }>
            <StatusBar barStyle={"light-content"} backgroundColor='#11212f' />
            <Header 
                loggedIn={ userInfo !== null }
                name={ profileInfo ? profileInfo.name : '' }
                lanHandler={()=> setShowLan(true)}
                callCenterHandler={() => Linking.openURL(`tel:${contactInfo?.phone}`)} />

            <View style={[styles.bannerBg, {
                height : biggestBanner ? biggestBanner.height + 50 : hp(23)
            }]} />
            <View style={{ marginTop: -(biggestBanner ? biggestBanner.height+40 : hp(21)) }}> 
                {
                    !loading && newBanners.length > 0 && (
                        <SlideImageHeader 
                            height={biggestBanner ? biggestBanner.height : hp(14)} 
                            data={newBanners} />
                    )
                }
                <Countdown 
                    countdownDate={ countdownDate }
                    countdownDay={ countdownDay ?? 0 }
                    countdownHour={ countdownDay ? countdownHour : 0 }
                    countdownMin={ countdownDay ? countdownMin : 0 }
                    countdownSec={ countdownDay ? countdownSec : 0 }
                    buyHandler={()=> {
                        if (countdownDay) {
                            setShowBuy(true)
                        } else {
                            ToastAndroid.show(local.cannotBuy, ToastAndroid.LONG)
                        }
                    }} />
            </View>

            <View style={styles.bottomContent}>
                {
                    !userInfo && (
                        <View style={styles.loginRow}>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                style={styles.loginBtn}
                                onPress={ openLoginModal }>
                                <Text style={[
                                    styles.loginTxt, 
                                    {
                                        fontSize: lang === 'en' ? hp(1.7) : hp(1.5)
                                    }
                                ]}>
                                    {local.login}
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                style={[styles.loginBtn, styles.regBtn]}
                                onPress={ openRegisterModal }>
                                <Text style={[
                                    styles.loginTxt, 
                                    {
                                        fontSize: lang === 'en' ? hp(1.7) : hp(1.5)
                                    }
                                ]}>
                                    {local.register}
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                style={styles.orBtn}>
                                <Text style={styles.orTxt}>
                                    {local.or}
                                </Text>
                            </TouchableOpacity>
                            <View style={styles.rotateBtn} />
                        </View>
                    )
                }
                
                <TicketHeader title={local.liveSaleTickets} />
                <Ticket 
                    countdownDay={ !countdownDay }
                    data={liveTickets}
                    drawDate={ drawDate }
                    live={ true }
                    pressHandler={(ticket) => {
                        setDefaultTicket(ticket);
                        if (countdownDay) {
                            setShowBuy(true)
                        }
                    }}
                    ticket_background={ liveTickets?.ticket_background }
                    ticket_photo={ liveTickets?.photo } />

                <TicketHeader title={local.lastDrawTickets} />
                <Ticket 
                    countdownDay={ false }  
                    data={lastDrawTickets}
                    drawDate={ drawDate }
                    pressHandler={() => navigation.navigate("ResultStack")}
                    ticket_background={ lastDrawTickets?.ticket_background }
                    ticket_photo={ lastDrawTickets?.photo } />
            </View>

            {
                showLan && <Language 
                    closeModalHandler={()=> setShowLan(false)}
                    lanHandler={ lanHandler } />
            }
            {
                showLogin && (
                    <LoginModal
                        onClose={()=> setShowLogin(false)}
                        defaultTab={ defaultTab } />
                )
            }
            {showBuy && <BuyTicket
                defaultTicket={ defaultTicket }
                onClose={()=> setShowBuy(false)}
                />}
            {loading && <Loading />}
        </ScrollView>
    )
}

export default Home;