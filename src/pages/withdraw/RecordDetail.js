import React, {
    useState,
    useContext
} from 'react';
import {
    View,
    Image,
    Text,
    ScrollView,
    TouchableOpacity,
    ToastAndroid
} from 'react-native';
import Octicons from 'react-native-vector-icons/Octicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';


import { Context } from '../../context/Provider';
import { useLocal } from '../../hook/useLocal';

import styles from './Style';
import Header from '../../components/header/DetailHeader';
import Loading from '../../components/loading/Loading';
import NetworkProblem from '../../components/modal/NetworkProblem';
import SessionExpired from '../../components/modal/SessionExpired';
import CustomAlert from '../../components/modal/CustomAlert';

const RecordDetailScreen = ({navigation, route}) => {

    const {record} = route.params;
    const local = useLocal();

    const { 
        net,
    } = useContext(Context);

    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        expired,
        setExpired
    ] = useState(false);
    const [
        alert,
        setAlert
    ] = useState();

    const statusColor = record.status === "Requested" ? "#e5a714" :
    record.status === "Completed" ? "#20cd43" : 
    record.status === "Rejected" ? "#ef5543" :
    record.status === "Refunded" ? "#567b99" :  "gray";

    const statusDesc = record.status === "Requested" ? local.requested :
    record.status === "Completed" ? local.completed : 
    record.status === "Rejected" ? local.rejected :
    record.status === "Refunded" ? local.refunded : "";

    return (
        <View style={styles.detailcontainer}>
 
            <Header
                title={local.recordDetail}
                goBack={()=> navigation.goBack()} />
            {!net ? <NetworkProblem /> : <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.topContent}>
                        {
                            record.status === "Completed" ? <Octicons
                                name='check-circle-fill'
                                color={statusColor}
                                size={hp(6)} /> : 
                            record.status === "Rejected" ? <MaterialIcons
                                name='error'
                                color={statusColor}
                                size={hp(6)} /> : 
                            record.status === "Requested" ?
                                <MaterialIcons
                                    name='pending'
                                    color={statusColor}
                                    size={hp(6)} /> :
                        <FontAwesome
                                name='get-pocket'
                                color={statusColor}
                                size={hp(6)} />
                        }
                        
                        <Text style={styles.detailPoint}>{record.point}</Text>

                        {/* <Text style={[styles.statusDesc, {color: statusColor}]}>
                            {statusDesc}
                        </Text> */}

                        {/* <View style={styles.toprow}>
                            <Image
                                source={{ uri : record.icon }}
                                style={styles.paymentIcon} />
                            <Text style={styles.account_type_detail}>{record.account_type}</Text>
                        </View>
                        <Text style={styles.detailPoint}>{record.point > 0 ? `+ ${record.point} ` : '0 '}Point</Text> */}
                    </View>

                    <View>
                        <View style={styles.infoRow}>
                            <Text style={styles.label}>{local.transactionId} :</Text>
                            <Text style={styles.value}>{record.transaction_id}</Text>
                        </View>
                        <View style={styles.infoRow}>
                            <Text style={styles.label}>{local.transferWith} :</Text>
                            <Text style={styles.value}>{record.option}</Text>
                        </View>
                        <View style={styles.infoRow}>
                            <Text style={styles.label}>{local.rechargePoint} :</Text>
                            <Text style={styles.value}>{record.point}</Text>
                        </View>
                        <View style={styles.infoRow}>
                            <Text style={styles.label}>{local.rechargeDate} :</Text>
                            <Text style={styles.value}>{record.date}</Text>
                        </View>
                        <View style={styles.infoRow}>
                            <Text style={styles.label}>{local.rechargeStatus} :</Text>
                            <Text style={[styles.value, {color: statusColor}]}>{statusDesc}</Text>
                        </View>
                   
                        {
                            record.qr_code ? <View>
                                <View style={styles.infoRow}>
                                    <Text style={styles.label}>{local.yourQr} :</Text>
                                </View>
                                <Image
                                    source={{ uri: record.qr_code }}
                                    style={styles.screenshot} />
                            </View> : null
                        }
                             {
                            record.screenshot ? <View>
                                <View style={styles.infoRow}>
                                    <Text style={styles.label}>{local.withdrawSS} :</Text>
                                </View>
                                <Image
                                    source={{ uri: record.screenshot }}
                                    style={styles.screenshot} />
                            </View> : null
                        }
                    </View>
 
                </ScrollView>}
            {loading && <Loading />}
        {
                expired && <SessionExpired
                    closeModalHandler={()=>  {
                        setExpired(false);
                        navigation.goBack();
                    }}/>
            }

        </View>
    )
}

export default RecordDetailScreen;