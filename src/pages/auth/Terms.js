import React, { useCallback, useEffect } from 'react';
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    BackHandler,
    Linking
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

import styles from './Style';
import { useLocal } from '../../hook/useLocal';

const Terms = ({
    onClose,
    acceptHandler,
    declineHandler
}) => {
    const local = useLocal();
    const backButtonHandler = useCallback(() => {
        return true;
      }, []);


  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backButtonHandler);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
    };
  }, [backButtonHandler]);

   
    return (
        <View style={[styles.modalView, {paddingTop: hp(2.5)}]}>
            <TouchableOpacity
                activeOpacity={0.8}
                style={ styles.close }
                onPress={ onClose }>
                <AntDesign
                    name='close'
                    size={hp(2.5)}
                    color='#11212f' />
            </TouchableOpacity>
            <ScrollView
                style={{paddingHorizontal: wp(3),marginBottom: acceptHandler ? 0 : hp(15)}}
                showsVerticalScrollIndicator={false}>
            
                <Text style={styles.title}>
                    စည်းမျဉ်းစည်းကမ်း သတ်မှတ်ချက်များ
                </Text>
                <Text style={styles.subtitle}>
                မင်္ဂလာပါ
                </Text>
                <Text style={styles.termsDesc}>
                    ShweTaik - Golden Castle တွင် ထိုင်း အစိုးရထီအား ဝယ်ယူကံစမ်းလိုသူများ အနေဖြင့် မိမိ၏ အကျိုးခံစားခွင့်များ လက်လွတ်ဆုံးရှုံးမှုများ မဖြစ်စေရန်အတွက် အောက်ပါစာပိုဒ်များ တွင် သတ်မှတ်ဖော်ပြထားသော စည်းမျဉ်းစည်းကမ်းများ ကို သေချာစွာ ဖတ်ရှုပြီးမှသာ ဝင်ရောက်ကံစမ်းစေလိုပါသည်။ သတ်မှတ်ဖော်ပြထားသော စည်းမျဉ်းစည်းကမ်းများအား လိုက်နာဆောင်ရွက်ရန် အဆင်မပြေပါက ယခုဝန်ဆောင်မှုအား အသုံးပြုရန် မသင့်တော်ပါ။
                </Text>

                <Text style={styles.subtitle}>
                    ၁။ အမည်နှင့် စကာရပ်များ၏ အဓိပ္ပာယ်ဖော်ပြချက်
                </Text>
                <Text style={styles.termsDesc}>
                    (က) ShweTaik - Golden Castle သည် ထိုင်းနိုင်ငံတွင် တစ်လလျှင် (၂) ကြိမ် တရားဝင် ဖွင့်လှစ် ရောင်းချနေသည့် ထိုင်းအစိုးရထီ  (နောင်တွင် ထိုင်းထီဟု ရည်ညွှန်း အသုံးပြုသွားပါမည်) အား အပြင်ထီလတ်မှတ်များ ဝယ်ယူရန် မလိုဘဲ အင်တာနက်မှ တစ်ဆင့် e-ticket အနေဖြင့် လွယ်ကူလျှင်မြန်စွာဖြင့် ကံစမ်းနိုင်စေရန် အာမခံမှုအပြည့်အဝဖြင့် ဝန်ဆောင်မှုပေးထားသော လုပ်ငန်းဖြစ်ပါသည်။ ShweTaik - Golden Castle ကုမ္ပဏီ ကို “ShweTaik” ၊ “Golden Castle”၊ “ကုမ္ပဏီ” ၊ “ကျွန်ုပ်တို့” ဟူ၍ လည်းကောင်းအလျဉ်းသင့်သလို ရည်ညွှန်းအသုံးပြုသွားမည်ဖြစ်ပါသည်။
                </Text>
                <Text style={styles.termsDesc}>
                    (ခ) ဝန်ဆောင်မှုများ ဆိုသည်မှာ ShweTaik Application မှတစ်ဆင့် ထိုင်းထီအား ထီကံစမ်းလိုသူများအတွက် အင်တာနက်ရှိယုံဖြင့် e-ticket ဖြင့် လွယ်ကူလျှင်မြန်စွာဖြင့် ကံစမ်းနိုင်ခြင်း၊ အကောင့်ဖွင့်စရာမလိုပဲ ထီတိုက်နိုင်ခြင်း၊ ထီပေါက်စဉ်အား ကြည့်ရှုခြင်းနှင့် ထိုင်းထီ ဖွင့်လှစ်ခြင်း အခမ်းအနားဗီဒီယိုအား တိုက်ရိုက်ပြသခြင်း အစရှိသည့် ShweTaik ကုမ္ပဏီ၏ ဝန်ဆောင်မှု များကို ဆိုလိုသည်။
                </Text>
                <Text style={styles.termsDesc}>
                    (ဂ) e-ticket ဆိုသည်မှာ မိမိထိုးလိုက်သော ထီလက်မှတ်အား အပြင်စာရွက်အနေနှင့် မဟုတ်ဘဲ ShweTaik Application ရှိ မိမိ ကိုယ်ပိုင်အကောင့်တွင်သာ Digital စနစ်ဖြင့် သိမ်းဆည်းထားသော ထီလက်မှတ် ကိုဆိုလိုသည်။
                </Text>

                <Text style={styles.subtitle}>
                ၂။ ဝန်ဆောင်မှုဆိုင်ရာ သဘောတူညီချက်
                </Text>
                <Text style={styles.termsDesc}>
                    (က) ဤ ShweTaik Application တွင် ထီကံစမ်းခြင်းဖြင့် လူကြီးမင်းမှ ShweTaik နှင့် သက်ဆိုင်သည့် စည်းမျဉ်း၊ စည်းကမ်းများ၊ ပြင်ဆင်ချက်များ၊ ဆုံးဖြတ်ချက်များအား လိုက်နာရန် သဘောတူညီသည်ဟု မှတ်ယူမည်ဖြစ်သည်။ 

                </Text>
                <Text style={styles.termsDesc}>
                    (ခ) ဆုကြေးငွေ နှင့်ပတ်သက်၍ မသာမှု (သို့) အငြင်းပွါးမှု တစုံတရာဖြစ်ပေါ်လာပါက ShweTaik ၏ ဆုံဖြတ်ချက်သည်သာ အတည်ဖြစ်သည်။
                </Text>

                <Text style={styles.termsDesc}>
                (ဂ) ShweTaik သည် မမျှော်မှန်းနိုင်သော သဘာဝဘေးန္တရာယ်တစ်ရပ်ရပ် ကြောင့်ဖြစ်စေ
ဖြစ်ပေါ်လာသော အခြေအနေအပေါ်မူတည်ပြီး အချိန်နှင့်တပြေးညီ စည်းမျဉ်းစည်းကမ်းများကို ပြင်ဆင်ပြောင်းလဲခွင့်ရှိသည်။ စည်းမျဉ်းစည်းကမ်းများ ပြင်ဆင်မှု တစ်စုံတစ်ရာရှိပါက ShweTaik ၏ Social Media Platform များမှဖြစ်စေ၊ ဤ ShweTaik Application မှဖြစ်စေ ကြော်ငြာမည်ဖြစ်ပြီး ကြော်ငြာသည်နှင့်တပြိုင်နက် အကျိုးသက်ရောက်မည်ဖြစ်သည်။ 
                </Text>
                <Text style={styles.termsDesc}>
                (ဃ) ကုမ္ပဏီမှ လူကြီးမင်းအား ကြိုတင် အကြောင်းကြားခြင်းမရှိဘဲ ကျွန်ုပ်တို့၏ website/ application အား ပြင်ဆင်ခြင်း၊ ပုံစံပြောင်းလဲခြင်းနှင့် အဆင့်မြှင့်တင်ခြင်း လုပ်ငန်းများအား လုပ်ပိုင်ခွင့်ရှိသည်။ 
                 </Text>
                <Text style={styles.termsDesc}>
                (င) ကုမ္ပဏီမှ လူကြီးမင်း၏ ကိုယ်ရေးကိုယ်တာအချက်အလက်များအား လုံခြုံမှုရှိစေရန်နှင့် လျှို့ဝှက်ထားမည် ဖြစ်ကြောင်း အာမခံပါသည်။
                </Text>

                <Text style={styles.subtitle}>
                ၃။ ထီကံစမ်းလိုသူများ အနေဖြင့်
                </Text>
                <Text style={styles.termsDesc}>
                (က) အကောင့်ဖွင့်ချိန်တွင် အသက် (၁၈) နှစ်ပြည့်ပြီးသူ (သို့) မိဘအုပ်ထိန်းသူ/တာဝန်ယူနိုင်သူ တစ်ဦးဦးမှ အကောင့်ဖွင့်ပေးပြီး ကံစမ်းမှုပြုလုပ်နိုင်ပါသည်။

</Text>
<Text style={styles.termsDesc}>
(ခ) ShweTaik Application အား Google play Store (သို့) Apple App Store (သို့) <Text style={styles.link} onPress={()=> Linking.openURL("https://goldencastlelottery.com")}>
https://goldencastlelottery.com
    </Text>  တွင် တိုက်ရိုက် အခမဲ့ ဒေါင်းယူအသုံးပြုနိုင်ပါသည်။

</Text>
<Text style={styles.termsDesc}>
(ဂ) ShweTaik Application တွင် ပွိုင့်(၁)ခု လျှင် မြန်မာငွေ (၁) ကျပ်နှုန်းဖြင့် ဝယ်ယူပြီး ထိုင်းထီ များအား ကာလပေါက်စျေးအတိုင်း e-ticket အနေဖြင့် ဝယ်ယူကံစမ်းနိုင်ပါသည်။ 
</Text>
<Text style={styles.termsDesc}>
(ဃ) ဝယ်ယူထားသည့် ပွိုင့်များကို အချိန်မရွေး Mobile Banking များမှ တစ်ဆင့် ငွေသားပြန်လည်ထုတ်ယူနိုင်ပါသည်။
</Text>
<Text style={styles.termsDesc}>
(င) ထီအစောင်အရေအတွက် အကန့်အသတ်မရှိ ကံစမ်းနိုင်သော်လည်း ထီနံပါတ်တစ်ခုလျှင် ဦးသူယူစနစ်ဖြင့် နံပါတ်တူအစောင်အရေအတွက် အများဆုံး (၃) စောင်အထိ ဝယ်ယူကံစမ်းနိုင်မည်ဖြစ်သည်။  
(မည်သူမဆို နံပါတ်တူ (၃) စောင်အထိ ထိုးပြီးပါက ထိုနံပါတ်အား အဆိုပါထီထွက်မည့် ကာလအတွင်း အခြားမည်သူမှ ရွေးချယ်ကံစမ်းခွင့်ရှိတော့မည်မဟုတ်ပါ)
</Text>
<Text style={styles.termsDesc}>
(စ) ထီပိတ်ချိန်မှာ ထီထွက်သည့်နေ့၏ မြန်မာစံတော်ချိန် နေ့လယ် (၁၂) နာရီ အတိိတိ တွင်ဖြစ်သည်။ အဆိုပါနေ့၏ နေ့လယ် (၁၂) နောက်ပိုင်း ငွေရှင်းရန် ကျန်ရှိနေသည့် ထီလက်မှတ်များသည် ထီကံထူးခွင့်ရရှိမည်မဟုတ်ဘဲ System မှ အလိုအလျောက် ပယ်ဖျက်သွားမည်ဖြစ်သည်။
 (ဝယ်မည့်စာရင်း (Add to Cart) လုပ်ထားသည့် ထီလက်မှတ်များအား ထီထွက်မည့်နေ့၏ နေ့လယ် (၁၂) နာရီ မတိုင်မှီ ငွေရှင်းထားရမည် ဖြစ်သည်)
</Text>
<Text style={styles.termsDesc}>
(ဆ) သင့်ကိုယ်ပိုင်အကောင့်အား သင်မှလွဲ၍ အခြားမည်သူမှ ဝင်ရောက်သုံးစွဲခြင်းမရှိပါ။အခြားသူဝင်ရောက်သုံးစွဲနေသည်ဟု သံသယရှိပါက လျှို့ဝှက်နံပါတ် ပြောင်းလဲခြင်း (သို့) ကုမ္ပဏီသို့ ဆက်သွယ်အကြောင်းကြားခြင်းများ ပြုလုပ်သင့်ပါသည်။
</Text>

<Text style={styles.subtitle}>
၄။ ထီကံထူးပါက
                </Text>
                <Text style={styles.termsDesc}>
                (က) ထီထွက်ပြီး (၁၅) ရက်အတွင်း သက်ဆိုင်ရာအေးဂျင့်များတစ်ဆင့်ဖြစ်စေ ကုမ္ပဏီ၏ Official Social Media Platform များဖြစ်သည့် (link) များ မှတစ်ဆင့် တိုက်ရိုက်ဆက်သွယ်ပြီး ဆုကြေးငွေထုတ်ယူနိုင်ပါသည်။ (သတ်မှတ်ချိန်ထက် ကျော်လွန်သွားသော ထီလက်မှတ်များအား ဆုကြေးငွေထုတ်ယူပေးသွားမည် မဟုတ်ပါ။)
                </Text>
                <Text style={styles.termsDesc}>
                (ခ) ဆုငွေထုတ်ယူရာတွင် ကံထူးရှင်အတည်ပြုနိုင်ရန် အတွက် ကံထူးသည့် e-ticket နှင့်အတူ Application ဖွင့်စဉ်က မှတ်ပုံတင်ထားသည့် အမည်၊ username အစရှိသည့် အချက်အလက်များ Update ဖြစ်အောင် ဖြည့်သွင်းပြသရမည်ဖြစ်ပါသည်။
                    </Text>
                    <Text style={styles.termsDesc}>
                    (ဂ) ဆုငွေများအား ထီထိုးစဉ်က သတ်မှတ်ထားသည့် ငွေလဲနှုန်းအတိုင်း မြန်မာငွေဖြင့် တွက်ချက်ပေးသွားမည်ဖြစ်သည်။ (ထီထွက်ပြီး ဖြစ်ပေါ်နေသည့် ကာလပေါက်စျေးအတိုင်း တွက်ချက်ပေးသွားမည် မဟုတ်ပါ။) 
                    </Text>
                    <Text style={styles.termsDesc}>
                    (ဃ) ဆုငွေများအား ဝန်ဆောင်ခ၊ အခွန် အစရှိသည့် မည်သည့် အပိုဖြတ်တောက်ခြင်းများမရှိဘဲ မြန်မာကျပ်ငွေဖြင့် အပြည့်အဝ ကံထူးနိုင်မည်ဖြစ်ပါသည်
                    </Text>
                    <Text style={styles.termsDesc}>
                    (င) ဆုကြေးငွေများအား ရုံးဖွင့်ရက် (၅)ရက်အတွင်း စီစဉ်ပေးသွားမည်ဖြစ်ပါသည်။

                    </Text>
                    <Text style={styles.termsDesc}>
                    (စ) ShweTaik Application တွင် e-ticket အနေဖြင့် ကံစမ်းထားသည့် ထီလက်မှတ်များကိုသာ ကုမ္ပဏီမှ ဆုကြေးငွေထုတ်ပေးသွားမည်ဖြစ်ပါသည်။ (ပြင်ပမှ မည်သည့်ထီလတ်မှတ်မျာကိုမှ ဆုကြေးငွေထုတ်ပေးသွားမည် မဟုတ်ပါ)
                    </Text>

                    <Text style={styles.link} onPress={()=> Linking.openURL("https://goldencastlelottery.com")}>
    goldencastlelottery.com
    </Text>

            </ScrollView>
            {
                acceptHandler && (
                    <View style={styles.termsFooter}>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={styles.footerBtn}
                            onPress={ acceptHandler }>
                            <Text style={[styles.acceptTxt, {color: '#fff'}]}>
                                {local.accept}
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={[styles.footerBtn, styles.declineBtn]}
                            onPress={ declineHandler }>
                            <Text style={[styles.acceptTxt, {color: '#ff9700'}]}>
                                {local.decline}
                            </Text>
                        </TouchableOpacity>
                    </View>
                )
            }
        </View>
    )
}

export default Terms;