import React, { useState, useContext, useEffect } from 'react';
import {
    View,
    Text,
    Modal,
    TextInput,
    TouchableOpacity,
    Image,
    StatusBar
} from 'react-native';
import Ionicons from "react-native-vector-icons/Ionicons";
import Feather from "react-native-vector-icons/Feather";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import FormData from 'form-data';

import { useLocal } from '../../hook/useLocal';
import { Context } from '../../context/Provider';
import { fetchPost } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { encryptData } from '../../utils/encryptData';
import { decryptData } from '../../utils/decryptData';
import { onlySpaces } from '../../utils/validation';
import { getItem, setItem } from '../../utils/appStorage';

import Loading from '../../components/loading/Loading';
import styles from './Style';
import Terms from './Terms';
import ErrorMessage from '../../components/errorMessage/ErrorMessage';

const Login = ({
    defaultTab,
    onClose
}) => {

    const local = useLocal();

    const {
        changeUserInfo,
        lang
    } = useContext(Context);
  
    const [
        loginShowPw,
        setLoginShowPw
    ] = useState(false);
    const [
        regShowPw,
        setRegShowPw
    ] = useState(false);
    const [
        regShowConfirmPw,
        setregShowConfirmPw
    ] = useState(false);
    const [
        tab,
        setTab
    ] = useState(defaultTab);
    const [
        name,
        setName
    ] = useState('');
    const [
        username,
        setUsername
    ] = useState('');
    const [
        password,
        setPassword
    ] = useState('');
    const [
        regUsername,
        setRegUsername
    ] = useState('');
    const [
        regPassword,
        setRegPassword
    ] = useState('');
    const [
        confirmPassword,
        setConfirmPassword
    ] = useState('');
    const [
        agentCode,
        setAgentCode
    ] = useState('');
    const [
        focused,
        setFocused
    ] = useState();
    const [
        agreed,
        setAgreed
    ] = useState(false);
    const [
        showTerms,
        setShowTerms
    ] = useState(false);
    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        userinfo,
        setUserinfo
    ] = useState(null);
    const [
        formError,
        setFormError
    ] = useState({
        nameError: null,
        usernameError: null,
        passwordError: null,
        confirmPwError: null,
        agentError: null,
        internalError: null
    })

    const goLogin = async () => {
        let valid = true;
        if(onlySpaces(username)) {
            valid = false;
            setFormError((prev) => (
                {
                    ...prev,
                    usernameError: local.invalidUsername
                }
            ));
        }
        if(onlySpaces(password)) {
            valid = false;
            setFormError((prev) => (
                {
                    ...prev,
                    passwordError: local.invalidPassword
                }
            ));
        }

        if (!valid) {
            return;
        }

        setLoading(true);

        let info = await getItem('@deviceInfo');
        let deviceInfo = JSON.parse(info);

        let fcmToken = await getItem('@fcmToken');

        let data = {
            device_name: deviceInfo._device_name,
            device_model: deviceInfo._device_model,
            os_version: deviceInfo._os_version,
            os_type: deviceInfo._os_type,
            app_version_id: deviceInfo._app_version,
            noti_token: fcmToken ? fcmToken : "FCM_TOKEN",
            language: 'en',
            user_name: username,
            password: password
        }

        let lgdata = encryptData(JSON.stringify(data));

        const response = await fetchPost(apiUrl.userLogin, {data: lgdata});
        
        if (response?.success && response?.data?.data) {
            setShowTerms(true);
            let decData = JSON.parse(decryptData(response.data.data));
            setUserinfo(decData);
        } else {
            if (response.data?.message) {
                setFormError((prev) => (
                    {
                        ...prev,
                        internalError: response.data.message
                    }
                ));
            }
        }
        setLoading(false);
    }

    const goRegister = async () => {

        let valid = true;
        if(onlySpaces(name)) {
            valid = false;
            setFormError((prev) => (
                {
                    ...prev,
                    nameError: local.invalidName
                }
            ));
        }
        if(onlySpaces(regUsername)) {
            valid = false;
            setFormError((prev) => (
                {
                    ...prev,
                    usernameError: local.invalidUsername
                }
            ));
        }
        if(onlySpaces(regPassword)) {
            valid = false;
            setFormError((prev) => (
                {
                    ...prev,
                    passwordError: local.invalidPassword
                }
            ));
        }
        if(onlySpaces(confirmPassword)) {
            valid = false;
            setFormError((prev) => (
                {
                    ...prev,
                    confirmPwError: local.invalidConfirmPassword
                }
            ));
        } else if(regPassword !== confirmPassword) {
            valid = false;
            setFormError((prev) => (
                {
                    ...prev,
                    confirmPwError: local.mismatchPassword
                }
            ));
        }

        if(onlySpaces(agentCode)) {
            valid = false;
            setFormError((prev) => (
                {
                    ...prev,
                    agentError: local.invalidAgentCode
                }
            ));
        }

        if (!valid) {
            return;
        }

        setLoading(true);

        let info = await getItem('@deviceInfo');
        let fcmToken = await getItem('@fcmToken');
        let deviceInfo = JSON.parse(info);

        let data = {
            device_name: deviceInfo._device_name,
            device_model: deviceInfo._device_model,
            os_version: deviceInfo._os_version,
            os_type: deviceInfo._os_type,
            app_version_id: deviceInfo._app_version,
            noti_token: fcmToken,
            language: 'en',
            name: name,
            user_name: regUsername,
            password: regPassword,
            password_confirmation: confirmPassword,
            agent_code: agentCode
        }

        let rgdata = encryptData(JSON.stringify(data));

        const response = await fetchPost(apiUrl.userRegister, {data: rgdata});

        if (response?.success && response?.data?.data) {
            setShowTerms(true);
            let decData = JSON.parse(decryptData(response.data.data));
            setUserinfo(decData);
        } else {
            if (response.data.errors) {
                setFormError((prev) => (
                    {
                        ...prev,
                        usernameError: response.data.errors.user_name ? response.data.errors.user_name[0] : null,
                        passwordError: response.data.errors.password ? response.data.errors.password[0] : null,
                    }
                ));
            } else if (response.data.message) {
                setFormError((prev) => (
                    {
                        ...prev,
                        agentError: response.data.message
                    }
                ));
            }
        }

        setLoading(false);
    }

    const closeTerms = () => {
        setShowTerms(false);
    }

    const acceptHandler = () => {
        setShowTerms(false);
        setItem('@userInfo', JSON.stringify(userinfo));

        changeUserInfo(userinfo);
        onClose();
    }

    const declineHandler = () => {
        setShowTerms(false);
    }

    const onChangeName = (val) => {
        setName(val);
        setFormError((prev) => (
            {
                ...prev,
                nameError: null,
                internalError: null
            }
        ));
    }

    const onChangeUsername = (val) => {
        setUsername(val);
        setFormError((prev) => (
            {
                ...prev,
                usernameError: null,
                internalError: null
            }
        ));
    }

    const onChangePassword = (val) => {
        setPassword(val);
        setFormError((prev) => (
            {
                ...prev,
                passwordError: null,
                internalError: null
            }
        ));
    }

    const onChangeRegUsername = (val) => {
        setRegUsername(val);
        setFormError((prev) => (
            {
                ...prev,
                usernameError: null,
                internalError: null
            }
        ));
    }

    const onChangeRegPassword = (val) => {
        setRegPassword(val);
        setFormError((prev) => (
            {
                ...prev,
                passwordError: null,
                internalError: null
            }
        ));
    }

    const onChangeConfirmPw = (val) => {
        setConfirmPassword(val);
        setFormError((prev) => (
            {
                ...prev,
                confirmPwError: null,
                internalError: null
            }
        ));
    }

    const onChangeAgentCode = (val) => {
        setAgentCode(val);
        setFormError((prev) => (
            {
                ...prev,
                agentError: null,
                internalError: null
            }
        ));
    }

    const changeTab = no => {
        setTab(no);
        setFormError((prev) => (
            {
                ...prev,
                nameError: null,
                usernameError: null,
                passwordError: null,
                confirmPwError: null,
                agentError: null,
                internalError: null
            }
        ));
    }

    return (
        <Modal
            transparent={false}
            visible={true}
            onRequestClose={onClose}
            animationType="fade">
            {
                showTerms ?
                    <Terms
                        onClose={ closeTerms }
                        acceptHandler={ acceptHandler }
                        declineHandler={ declineHandler } /> :
                    <View 
                        style={styles.modalView}>
                        <StatusBar backgroundColor="#11212f" barStyle='light-content' />
                        <View style={styles.upperContainer}>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                style={styles.backBtn}
                                onPress={onClose}>
                                <Ionicons
                                    name="ios-return-up-back"
                                    size={hp(3.5)}
                                    color="#fff" />
                            </TouchableOpacity>
                            <Text style={lang === 'en' ? styles.welcome : styles.appname}>
                                {local.welcomeTo}
                            </Text>
                            <Text style={lang === 'en' ? styles.appname : styles.welcome}>
                                {local.appName}
                            </Text>
                            <View style={styles.logo2}>
                                <Image
                                    source={require('../../assets/images/logo_only.png')}
                                    style={styles.logo} />
                            </View>
                        </View>
                        <View style={styles.card}>
                            <View style={styles.headerRow}>
                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    style={styles.headerBtn}
                                    onPress={ ()=> changeTab(1) }>
                                    <Text style={tab === 1 ? styles.activeHeaderTxt : styles.headerTxt}>
                                        { local.login }
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    style={styles.headerBtn}
                                    onPress={ ()=> changeTab(2) }>
                                    <Text style={tab === 2 ? styles.activeHeaderTxt : styles.headerTxt}>
                                    { local.register }
                                    </Text>
                                </TouchableOpacity>
                            </View>

                            <View style={styles.inputContainer}>
                                {
                                    tab === 2 && (
                                        <View style={styles.inputContent}>
                                            <Feather
                                                name="user"
                                                size={wp(5)}
                                                color={focused === 6 ? "#ff9700" : '#c9c7c3'}
                                                style={styles.icon} />
                                            <TextInput
                                                placeholder={local.name}
                                                placeholderTextColor="#c9c7c3"
                                                value={name}
                                                onChangeText={ onChangeName }
                                                maxLength={30}
                                                onFocus={()=> setFocused(6)}
                                                onBlur={()=> setFocused(null)}
                                                style={[styles.inputText, {
                                                    borderColor: focused === 6 ? "#ff9700" : 'lightgray',
                                                    fontSize: username === "" ? hp(1.5) : hp(1.7)
                                                }]} />
                                        </View>
                                    )
                                }
                                <ErrorMessage message={formError.nameError} />

                                <View style={[styles.inputContent, {marginTop: tab === 1 ? 0 : hp(2)}]}>
                                    <Feather
                                        name="user"
                                        size={wp(5)}
                                        color={focused === 1 ? "#ff9700" : '#c9c7c3'}
                                        style={styles.icon} />
                                    <TextInput
                                        placeholder={local.username}
                                        placeholderTextColor="#c9c7c3"
                                        value={tab === 1 ? username : regUsername}
                                        onChangeText={ tab === 1 ? onChangeUsername : onChangeRegUsername }
                                        maxLength={30}
                                        onFocus={()=> setFocused(1)}
                                        onBlur={()=> setFocused(null)}
                                        style={[styles.inputText, {
                                            borderColor: focused === 1 ? "#ff9700" : 'lightgray',
                                            fontSize: username === "" ? hp(1.5) : hp(1.7)
                                        }]} />
                                </View>
                                <ErrorMessage message={formError.usernameError} />

                                <View style={[styles.inputContent, {marginTop: hp(2)}]}>
                                    <Feather
                                        name="lock"
                                        size={wp(5)}
                                        color={focused === 2 ? "#ff9700" : '#c9c7c3'}
                                        style={styles.icon} />
                                    <TextInput
                                        placeholder={local.password}
                                        placeholderTextColor="#c9c7c3"
                                        value={tab === 1 ? password : regPassword}
                                        onChangeText={ tab === 1 ? onChangePassword : onChangeRegPassword }
                                        secureTextEntry={tab === 1 ? !loginShowPw : !regShowPw}
                                        maxLength={30}
                                        onFocus={()=> setFocused(2)}
                                        onBlur={()=> setFocused(null)}
                                        style={[styles.inputText, {
                                            borderColor: focused === 2 ? "#ff9700" : 'lightgray',
                                            fontSize: password === "" ? hp(1.5) : hp(1.7)
                                        }]} />
                                    <TouchableOpacity 
                                        style={styles.eyeIcon}
                                        onPress={()=> {
                                            if (tab === 1) {
                                                setLoginShowPw(!loginShowPw)
                                            } else {
                                                setRegShowPw(!regShowPw)
                                            }
                                        }}>
                                        <Ionicons
                                            name={((tab === 1 && loginShowPw) || (tab === 2 && regShowPw)) ? "eye" : "eye-off" }
                                            size={wp(5)}
                                            color={focused === 2 ? "#ff9700" : '#c9c7c3'} />
                                    </TouchableOpacity>
                                </View>
                                <ErrorMessage message={formError.passwordError} />

                                {
                                    tab === 2 && (
                                        <>
                                            <View style={[styles.inputContent, {marginTop: hp(2)}]}>
                                                <Feather
                                                    name="lock"
                                                    size={wp(5)}
                                                    color={focused === 4 ? "#ff9700" : '#c9c7c3'}
                                                    style={styles.icon} />
                                                <TextInput
                                                    placeholder={local.confirmPassword}
                                                    secureTextEntry={!regShowConfirmPw}
                                                    placeholderTextColor="#c9c7c3"
                                                    value={confirmPassword}
                                                    onChangeText={ onChangeConfirmPw }
                                                    maxLength={30}
                                                    onFocus={()=> setFocused(4)}
                                                    onBlur={()=> setFocused(null)}
                                                    style={[styles.inputText, {
                                                        borderColor: focused === 4 ? "#ff9700" : 'lightgray',
                                                        fontSize: password === "" ? hp(1.5) : hp(1.7)
                                                    }]} />
                                                <TouchableOpacity 
                                                    style={styles.eyeIcon}
                                                    onPress={()=> {
                                                        setregShowConfirmPw(!regShowConfirmPw)
                                                    }}>
                                                    <Ionicons
                                                        name={regShowConfirmPw ? "eye" : "eye-off" }
                                                        size={wp(5)}
                                                        color={focused === 4 ? "#ff9700" : '#c9c7c3'} />
                                                </TouchableOpacity>
                                            </View>
                                            <ErrorMessage message={formError.confirmPwError} />
                                            <View style={[styles.inputContent, {marginTop: hp(2)}]}>
                                                <MaterialCommunityIcons
                                                    name="face-agent"
                                                    size={wp(5.5)}
                                                    color={focused === 3 ? "#ff9700" : '#c9c7c3'}
                                                    style={styles.icon} />
                                                <TextInput
                                                    placeholder={local.agentCode}
                                                    placeholderTextColor="#c9c7c3"
                                                    value={agentCode}
                                                    onChangeText={ onChangeAgentCode }
                                                    maxLength={30}
                                                    onFocus={()=> setFocused(3)}
                                                    onBlur={()=> setFocused(null)}
                                                    style={[styles.inputText, {
                                                        borderColor: focused === 3 ? "#ff9700" : 'lightgray',
                                                        fontSize: agentCode === "" ? hp(1.5) : hp(1.7)
                                                    }]} />
                                            </View>
                                            <ErrorMessage message={formError.agentError} />
                                        </>    
                                    )
                                }
                            </View>
                          
                            <ErrorMessage message={formError.internalError} />
                            <TouchableOpacity 
                                activeOpacity={0.8}
                                style={styles.loginBtn}
                                onPress={ tab === 1 ? goLogin : goRegister }>
                                <LinearGradient
                                    colors={[
                                        'rgba(17, 33, 47, 0.5)',
                                        'rgba(17, 33, 47, 0.6)',
                                        'rgba(17, 33, 47, 0.9)',
                                        'rgba(17, 33, 47, 1)',
                                        'rgba(17, 33, 47, 0.6)',
                                        'rgba(17, 33, 47, 0.5)'
                                    ]}
                                    start={{ x: 0.1, y: 0.06 }}
                                    style={styles.gradientBg}>
                                    <Text style={styles.submitTxt}>
                                        { tab === 1 ? local.login : local.register }
                                    </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View> 
                    </View>
            }
            {
                loading && <Loading />
            }
        </Modal>
    )
}

export default Login;