import React, {
    useEffect
} from 'react';
import {
    View,
    StatusBar,
    Text,
    Image
} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

import styles from './Style';
import Logo from '../../assets/icons/Logo';

const Splash = ({navigation}) => {

    return (
        <View style={styles.container}>
            <StatusBar barStyle={'light-content'} backgroundColor="#11212f" />
            {/* <Logo size={wp(40)} /> */}
            <Image 
                                    source={require('../../assets/images/logo_only.png')}
                                    style={styles.logoProfile}/>
            
            <Text style={styles.name}>
                SHWE TAIK
            </Text>
        </View>
    )
}

export default Splash;