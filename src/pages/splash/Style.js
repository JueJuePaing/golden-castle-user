import { StyleSheet } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#11212f'
    },
    name: {
        color: '#ff9700',
        paddingTop: hp(2),
        fontFamily: 'Poppins-Medium',
        fontSize: hp(2)
    },
    logoProfile: {
        width: wp(40),
        height: hp(35),
        resizeMode: 'contain'
    }
})

export default styles;