import { StyleSheet, StatusBar } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginBottom: hp(8)
    },
    nodata: {
        height: hp(100),
        width: wp(100),
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute'
    },
    accounts : {
        paddingHorizontal : wp(3)
    },
    accountBtn: {
        paddingHorizontal : wp(3),
        paddingVertical: hp(.5),
        borderRadius: hp(.5),
        borderWidth: 1,
        marginRight: wp(2),
        marginVertical: hp(1.6)
    },
    selectedAccountBtn: {
        backgroundColor: '#11212f',
        borderWidth: 0
    },
    selectedAccountName: {
        color: '#fff',
        fontSize: hp(1.8),
    },
    checked: {
        position: 'absolute',
        right: 0,
        top: -hp(0.3),
        zIndex: 5
    },
    qrContent: {
        alignSelf: 'center',
        marginTop: hp(5)
    },
    qrcode: {
        height: hp(30),
        width: wp(70),
        resizeMode: 'contain'
    },
    title: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.7),
        textAlign: 'center',
        paddingBottom: hp(1.5),
        color : "#11212f"
    },
    phone_number: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.85),
        textAlign: 'center',
        color: '#11212f'
    },
    or: {
        // fontFamily: 'Poppins-Regular',
        fontSize: hp(1.5),
        textAlign: 'center',
        paddingVertical: hp(1.5),
        color : "#11212f"
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        paddingTop: hp(2.5)
    },
    copyBtn: {
        marginLeft: wp(2),
        marginBottom: hp(1)
    },
    submitBtn: {
        alignSelf: 'center',
        height: hp(4.5),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#11212F',
        borderRadius: hp(3),
        // marginTop: hp(5),
        paddingHorizontal: wp(6),
        borderWidth: 1,
        borderColor: '#FF9700',

        shadowOffset: {width: 5, height: 10},
        shadowColor: '#fff',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3,
        // marginBottom : hp(2)

        position: 'absolute',
        bottom: hp(14)
    },
    submitTxt: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.8),
        color: '#fff',
        paddingTop: hp(.5)
    },
    file: {
        alignSelf: 'center',
        height: hp(65),
        width: wp(90),
        marginTop: hp(2),
        resizeMode: 'contain'
    },
    footer: {
       alignSelf: 'center',
        flexDirection: 'row',
        marginTop: hp(3),
        // marginRight: wp(5)
    },
    confirmBtn: {
        height: hp(4.5),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#11212F',
        borderRadius: hp(1),
        borderWidth: 1,
        borderColor: '#FF9700',

        shadowOffset: {width: 5, height: 10},
        shadowColor: '#fff',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3,
        paddingHorizontal: wp(6)
        //width: wp(35)
    },
    confirmTxt: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.8),
        color: '#fff',
        paddingTop: hp(.5),
        textTransform: 'uppercase'
    },
    cancelBtn: {
        height: hp(4.5),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: hp(1),
       // borderWidth: 0.7,
        borderColor: '#11212f',

        shadowOffset: {width: 5, height: 10},
        shadowColor: '#fff',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3,
        //paddingHorizontal: wp(2),
        marginRight: wp(3)
       //  width: wp(35)
    },
    cancelTxt: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.8),
        color: '#11212f',
        paddingTop: hp(.5),
        textTransform: 'uppercase'
    },
    mainContent: {
        marginVertical: hp(2),
        alignSelf: 'center'
    },
    point: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.7),
        color: '#ff9700',
    },
    topRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    record: {
        width: wp(94),
        backgroundColor: '#fff',
        borderRadius: hp(.5),
        paddingHorizontal: wp(3),
        paddingVertical: hp(1.5),
        marginBottom: hp(1)
    },
    account_type: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.7),
        color: '#000',
    },
    recordRow: {
        flexDirection: 'row',
        width: wp(90),
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: hp(.3)
    },
    statusRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    status: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.5),
        paddingLeft: wp(1)
    },
    recordDate: {
        fontFamily: 'Poppins-Regular',
        fontSize: hp(1.4),
        color:'darkgray'
    },
    statusDot: {
        width: wp(2),
        height: wp(2),
        borderRadius: wp(2),
    },
    accountInfo : {
        width: wp(90),
        paddingHorizontal: wp(3),
        paddingVertical: hp(1.5),
        borderRadius: hp(1),
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    accountInfo2: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    accountName : {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.6),
        paddingTop: hp(.3),
        color: '#11212f'
    },
    accountIcon: {
        width: wp(13),
        height: wp(10),
        marginRight: wp(2),
        resizeMode: 'contain'
    },
    bankIcon: {
        width: wp(5),
        height: wp(5),
        resizeMode: 'contain',
        marginRight: wp(1)
    },
    bankRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },

    // Detail
    detailcontainer: {
        flex: 1,
        marginBottom: hp(8),
        // alignItems: 'center'
    },
    topContent: {
        backgroundColor: '#11212f',
        width: wp(92),
        paddingVertical: hp(2),
        borderRadius: hp(1),
        alignSelf: 'center',
        alignItems: 'center',
        paddingTop: hp(3),
        marginTop: hp(1.5)
    },
    account_type_detail: {
        color: '#fff',
        fontFamily: 'Poppins-Medium'
    },
    toprow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    paymentIcon: {
        width: wp(12),
        height: wp(12),
        borderRadius: wp(12),
        marginRight: wp(2)
    },
    detailPoint: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(2.7),
        color: '#ff9700',
        paddingTop: hp(1.7)
    },
    infoRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: wp(92),
        alignSelf: 'center',
        marginTop: hp(3)
    },
    label: {
        fontFamily: 'Poppins-Regular',
        color: 'darkgray',
        fontSize: hp(1.8)
    },
    value: {
        fontFamily: 'Poppins-Medium',
        color: '#000',
        fontSize: hp(1.7)
    },
    screenshot: {
        alignSelf: 'center',
        width: wp(70),
        height: hp(60),
        resizeMode: 'cover',
        marginTop: hp(2),
        borderRadius: hp(.5),
        marginBottom: hp(5)
    },
    statusDesc: {
        fontFamily: 'Poppins-Regular',
        color: '#fff',
        fontSize: hp(1.7),
        
        // paddingVertical: hp(1.2)
    }
})

export default styles;