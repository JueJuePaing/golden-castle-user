import React, {
    useState,
    useContext
} from 'react';
import {
    View,
    Image,
    Text,
    TouchableOpacity,
    ToastAndroid
} from 'react-native';

import { fetchPostImageUpload } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { Context } from '../../context/Provider';
import { deleteMultiItems } from '../../utils/appStorage';
import { useLocal } from '../../hook/useLocal';
import { encryptData } from '../../utils/encryptData';
import { decryptData } from '../../utils/decryptData';

import styles from './Style';
import Header from '../../components/header/DetailHeader';
import Loading from '../../components/loading/Loading';
import NetworkProblem from '../../components/modal/NetworkProblem';
import SessionExpired from '../../components/modal/SessionExpired';
import CustomAlert from '../../components/modal/CustomAlert';

const ReceiptScreen = ({navigation, route}) => {

    const {file, selectedAccount} = route.params;
    const local = useLocal();

    const { 
        lang,
        net,
        userInfo,
        changeUserInfo
    } = useContext(Context);

    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        expired,
        setExpired
    ] = useState(false);
    const [
        alert,
        setAlert
    ] = useState();

    const logoutHandler = async () => {
        await deleteMultiItems(['@userInfo']);
        changeUserInfo(null);
    }

    const confirmHandler = async () => {

        if (userInfo && userInfo.access_token) {
            setLoading(true);

            const uploadData = new FormData();
            let data = {
                account_id: selectedAccount.id
            }
            let encData = encryptData(JSON.stringify(data), userInfo.secret_key);
            uploadData.append('data', encData);
            uploadData.append('screenshot', {
                type: file.type,
                uri: file.uri,
                name: file.fileName,
                width: file.width,
                height: file.height,
            });

            const response = await fetchPostImageUpload(
                apiUrl.recharge,
                uploadData,
                userInfo.access_token,
            );

            setLoading(false);

            if (response?.success) {
                ToastAndroid.show(response.message, ToastAndroid.SHORT);
                navigation.goBack();
            } else {
                if (response?.status === 401) {
                    // Unauthenticated
                    logoutHandler();
                    setExpired(true);
                } else {
                    if (response?.message) {
                        setAlert(response.message)
                    } else {
                        setAlert(local.somethingWrong)
                    }
                }
            }
    } else {
        ToastAndroid.show(local.unauthenticated, ToastAndroid.SHORT);
        logoutHandler();
        navigation.goBack();
    }
    }

    const cancelHandler = () => {
        navigation.goBack();
    }

    return (
        <View style={styles.container}>
 
            <Header
                title={local.rechargeSS}
                goBack={()=> navigation.goBack()} />
            {
                !net ?
                    <NetworkProblem /> :
                    <Image
                        source={{ uri: file.uri }}
                        style={ styles.file } />
            }
            {loading && <Loading />}
            {
                expired && <SessionExpired
                    closeModalHandler={()=>  {
                        setExpired(false);
                        navigation.goBack();
                    }}/>
            }
             {
                alert && <CustomAlert
                    message={alert}
                    closeHandler={()=>  {
                        setAlert()
                    }}/>
            }

            <View style={styles.footer}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={ styles.cancelBtn }
                    onPress={ cancelHandler }>
                    <Text style={styles.cancelTxt}>{local.cancel}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={ styles.confirmBtn }
                    onPress={ confirmHandler }>
                    <Text style={styles.confirmTxt}>{local.confirm}</Text>
                </TouchableOpacity>
            </View>

        </View>
    )
}

export default ReceiptScreen;