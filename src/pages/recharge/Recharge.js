import React, {
    useState,
    useContext,
    useEffect
} from 'react';
import {
    View,
    Image,
    Text,
    ScrollView,
    TouchableOpacity,
    ToastAndroid
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign'
import Ionicons from 'react-native-vector-icons/Ionicons';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { launchImageLibrary } from "react-native-image-picker";
import Clipboard from "@react-native-community/clipboard";

import { fetchPostByToken } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { Context } from '../../context/Provider';
import { deleteMultiItems } from '../../utils/appStorage';
import { useLocal } from '../../hook/useLocal';
import { encryptData } from '../../utils/encryptData';
import { decryptData } from '../../utils/decryptData';

import styles from './Style';
import Header from '../../components/header/DetailHeader';
import Loading from '../../components/loading/Loading';
import NoData from '../../components/noData/NoData';
import NetworkProblem from '../../components/modal/NetworkProblem';
import SessionExpired from '../../components/modal/SessionExpired';
import AccountModal from '../../components/AccountModal/AccountModal';

const RechargeScreen = ({navigation}) => {

    const { 
        lang,
        net,
        userInfo,
        changeUserInfo
    } = useContext(Context);

    const local = useLocal();

    const lanData = {
        language: lang
    }

    const [
        accounts,
        setAccounts
    ] = useState(null);
    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        noData,
        setNoData
    ] = useState(false);
    const [
        expired,
        setExpired
    ] = useState(false);
    const [
        selectedAccount,
        setSelectedAccount
    ] = useState();
    const [
        showModal,
        setShowModal
    ] = useState(false);

    useEffect(() => {
        if (net) getAccounts();
    }, [net]);

    const getAccounts = async () => {

        if (userInfo && userInfo.access_token) {
            setLoading(true);

            const encryptLanData = encryptData(JSON.stringify(lanData), userInfo.secret_key);
            const response = await fetchPostByToken(apiUrl.rechargeAccounts, {data: encryptLanData}, userInfo.access_token);

            setLoading(false);
            if (response?.success && response?.data) {
                const decRecharge = JSON.parse(decryptData(response.data, userInfo.secret_key));
                if(decRecharge?.accounts) {
                    setAccounts(decRecharge.accounts);
                    setSelectedAccount({...decRecharge.accounts[0]});
                }
            } else {
                if (response?.status === 401) {
                    // Unauthenticated
                    logoutHandler();
                    setExpired(true);
                } else {
                    if (response.message) {
                        ToastAndroid.show(response.message, ToastAndroid.SHORT);
                    } else {
                        ToastAndroid.show(local.somethingWrong, ToastAndroid.SHORT);
                    }
                }
            }
        } else {
            ToastAndroid.show(local.unauthenticated, ToastAndroid.SHORT);
            logoutHandler();
            navigation.goBack();
        }
    }

    const logoutHandler = async () => {
        await deleteMultiItems(['@userInfo']);
        changeUserInfo(null);
    }

    const chooseHandler = (account, index) => {
        setShowModal(false);
        setSelectedAccount(account);
    }

    const accountHandler = (account) => {
        setShowModal(true);
    }

    const copiesHandler = item => {
        Clipboard.setString(`${selectedAccount.phone_number}`);
        ToastAndroid.show(local.copied, ToastAndroid.SHORT);
    };

    const uploadHandler = () => {
        launchImageLibrary({
            selectionLimit : 1,
            mediaType : "photo"
        }, response => {
            if (response.didCancel !== true) {
              if (response && response.assets && response.assets.length > 0) {
                navigation.navigate("Receipt", {file: response.assets[0], selectedAccount});
              } else {
                ToastAndroid.show(local.somethingWrong, ToastAndroid.SHORT);
              }
            }
          });
    }

    const rightBtnHandler = () => {
        navigation.navigate("RechargeHistory")
    }

    return (
        <View style={styles.container}>
 
            <Header
                title={local.recharge}
                goBack={()=> navigation.goBack()}
                rightBtn={local.history}
                rightBtnHandler={ rightBtnHandler } />
            {
                !net ?
                    <NetworkProblem /> :
                    <>
                        {!loading && accounts && (
                            <ScrollView 
                                style={ styles.mainContent }
                                showsVerticalScrollIndicator={false} >
                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    style={styles.accountInfo}
                                    onPress={ accountHandler }>
                                    <View style={styles.accountInfo2}>
                                        <Image
                                            source={{ uri : selectedAccount?.icon }}
                                            style={ styles.accountIcon } />
                                        <Text style={ styles.accountName }>
                                            { selectedAccount?.name }
                                        </Text>
                                    </View>
                                    <AntDesign
                                        name="downcircleo"
                                        size={hp(2)}
                                        color='#11212f'
                                        />
                                </TouchableOpacity>

                                <View style={ styles.qrContent }>
                                    <Text style={ styles.title }>
                                        {local.scanToPay}
                                    </Text>
                                    
                                    <Image
                                        source={{ uri : selectedAccount?.qr_code ? selectedAccount.qr_code : selectedAccount?.icon }}
                                        style={ styles.qrcode } />
                                    {/* <Text style={ styles.or }>
                                        (OR)
                                    </Text> */}
                                    <View style={styles.row}>
                                        <Text style={ styles.phone_number }>
                                        { selectedAccount?.account_name ? `${selectedAccount.account_name}` : "" }
                                        </Text>
                                        <TouchableOpacity
                                            activeOpacity={0.8}
                                            style={ styles.copyBtn }
                                            onPress={ copiesHandler }>
                                            <Ionicons
                                                name="copy-outline"
                                                size={hp(2)}
                                                color="#ff9700" />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </ScrollView>
                        ) }
                            
                        {loading && <Loading />}

                        {
                            !loading && noData && <View  style={styles.nodata}>
                                <NoData message={local.noRechargeAccount} />
                            </View>
                        }
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={ styles.submitBtn }
                            onPress={ uploadHandler }>
                            <Text style={styles.submitTxt}>
                                {local.uploadReceipt}
                            </Text>
                        </TouchableOpacity>
                    </>
            }
            {
                expired && <SessionExpired
                    closeModalHandler={()=>  {
                        setExpired(false);
                        navigation.goBack();
                    }}/>
            }
            {
                showModal && (
                    <AccountModal
                        recharge={true}
                        accounts={accounts}
                        selectedAccount={selectedAccount}
                        closeModalHandler={()=> setShowModal(false)}
                        chooseHandler={ chooseHandler } />
                )
            } 

        </View>
    )
}

export default RechargeScreen;