import React, {
    useState,
    useContext,
    useEffect
} from 'react';
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    ToastAndroid
} from 'react-native';
import { deleteMultiItems } from '../../utils/appStorage';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp
} from 'react-native-responsive-screen';

import { fetchPostByToken } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { onlySpaces } from '../../utils/validation';
import { Context } from '../../context/Provider';
import { useLocal } from '../../hook/useLocal';
import { encryptData } from '../../utils/encryptData';
import { decryptData } from '../../utils/decryptData';

import styles from './Style';
import Header from '../../components/header/DetailHeader';
import Loading from '../../components/loading/Loading';
import ErrorMessage from '../../components/errorMessage/ErrorMessage';
import SessionExpired from '../../components/modal/SessionExpired';

const EditProfile = ({navigation, route}) => {

    const {profileData} = route.params;
    const local = useLocal();
    const { 
        userInfo,
        net,
        changeUserInfo,
        changeProfileInfo
    } = useContext(Context);

    const [
        expired,
        setExpired
    ] = useState(false);
    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        name,
        setName
    ] = useState('')
    const [
        phone,
        setPhone
    ] = useState('')
    const [
        nrc,
        setNrc
    ] = useState('')
    const [
        focused,
        setFocused
    ] = useState()
    const [
        formError,
        setFormError
    ] = useState({
        nameError: null,
        internalError: null
    })

    useEffect(() => {
        if (profileData) {
            setName(profileData.name)
            setPhone(profileData.phone_number)
            setNrc(profileData.nrc)
        }
    }, [profileData]);

    const onClose = () => {
        navigation.goBack();
    }

    const submitHandler = async () => {
        if (!net) {
            ToastAndroid.show(local.noConnection, ToastAndroid.SHORT);
            return;
        }
        
        let valid = true;
        if(!name || onlySpaces(name)) {
            valid = false;
            setFormError((prev) => (
                {
                    ...prev,
                    nameError: local.invalidName
                }
            ));
        }
        if (!valid) {
            return;
        }

        setLoading(true);

        const data = {
            name
        }
        if (phone) {
            data['phone_number'] = phone;
        }
        if (nrc) {
            data['nrc'] = nrc;
        }

        if (userInfo?.access_token) {
            let encryptProfileData = encryptData(JSON.stringify(data), userInfo.secret_key);
            const response = await fetchPostByToken(apiUrl.updateProfile, {data: encryptProfileData}, userInfo.access_token);
            setLoading(false);
            if (response?.success && response?.data) {
                const decUserInfo = JSON.parse(decryptData(response.data, userInfo.secret_key));
                changeProfileInfo(decUserInfo.user);
                ToastAndroid.show("Success!", ToastAndroid.SHORT);
                navigation.goBack();
            } else {
                if (response?.status === 401) {
                    // Unauthenticated
                    logoutHandler();
                    setExpired(true);
                } else {
                    if (response?.message) {
                        setFormError((prev) => (
                            {
                                ...prev,
                                internalError: response.message
                            }
                        ));
                    } else {
                        ToastAndroid.show("Something went wrong. Try again!", ToastAndroid.SHORT);
                    }
                }
            }
        } else {
            logoutHandler();
            setExpired(true);
        }
    }

    const logoutHandler = async () => {
        await deleteMultiItems(['@userInfo']);
        changeUserInfo(null);
    }


    const onChangeName = (val) => {
        setName(val);
        setFormError((prev) => (
            {
                ...prev,
                nameError: null,
                internalError: null
            }
        ));
    }

    const onChangePhone = (val) => {
        setPhone(val);
        setFormError((prev) => (
            {
                ...prev,
                internalError: null
            }
        ));
    }

    const onChangeNRC = (val) => {
        setNrc(val);
        setFormError((prev) => (
            {
                ...prev,
                internalError: null
            }
        ));
    }
    return (
        <View style={styles.container}>
            <Header
                title={local.updateProfile}
                goBack={onClose}/>
            
            <View>
                <View style={[styles.inputContent, {marginTop: hp(2)}]}>
                    <TextInput
                        placeholder={local.name}
                        placeholderTextColor="#c9c7c3"
                        value={name}
                        onChangeText={ onChangeName }
                        maxLength={30}
                        onFocus={()=> setFocused(1)}
                        onBlur={()=> setFocused(null)}
                        style={[styles.inputText, {
                            borderColor: focused === 1 ? "#ff9700" : 'lightgray',
                            fontSize: name === "" ? hp(1.5) : hp(1.7)
                        }]} />
                </View>
                <ErrorMessage message={formError.nameError} />

                <View style={[styles.inputContent, {marginTop: hp(2)}]}>
                    <TextInput
                        placeholder={local.phone}
                        placeholderTextColor="#c9c7c3"
                        value={phone}
                        onChangeText={ onChangePhone }
                        maxLength={30}
                        onFocus={()=> setFocused(2)}
                        onBlur={()=> setFocused(null)}
                        keyboardType='numeric'
                        style={[styles.inputText, {
                            borderColor: focused === 2 ? "#ff9700" : 'lightgray',
                            fontSize: phone === "" ? hp(1.5) : hp(1.7)
                        }]} />
                </View>

                <View style={[styles.inputContent, {marginTop: hp(2)}]}>
                    <TextInput
                        placeholder={local.nrc}
                        placeholderTextColor="#c9c7c3"
                        value={nrc}
                        onChangeText={ onChangeNRC }
                        maxLength={30}
                        onFocus={()=> setFocused(3)}
                        onBlur={()=> setFocused(null)}
                        style={[styles.inputText, {
                            borderColor: focused === 3 ? "#ff9700" : 'lightgray',
                            fontSize: nrc === "" ? hp(1.5) : hp(1.7)
                        }]} />
                </View>
                <ErrorMessage message={formError.internalError} />

                <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.submitBtn}
                    onPress={ submitHandler }>
                    <Text style={styles.submitTxt}>{local.submit}</Text>
                </TouchableOpacity>
            </View>
            {
                loading && <Loading />
            }
            {
                expired && <SessionExpired
                    closeModalHandler={()=>  {
                        setExpired(false);
                        navigation.goBack();
                    }}/>
            }
        </View>
    )
}

export default EditProfile;