import React, {
    useState,
    useContext,
    useEffect
} from 'react';
import {
    View,
    Image,
    Text,
    TouchableOpacity,
    Linking,
    StatusBar
} from 'react-native';
import {
    heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import AntDesign from 'react-native-vector-icons/AntDesign'

import apiUrl from '../../utils/apiUrl';
import { fetchPost } from '../../utils/fetchData';
import {useLocal} from "../../hook/useLocal";
import styles from './Style';
import { Context } from '../../context/Provider';
import { encryptData } from '../../utils/encryptData';
import { decryptData } from '../../utils/decryptData';

const ContactUs = ({navigation, route}) => {

    const {agent_phone} = route.params;
    const local = useLocal();
    const { 
        lang
    } = useContext(Context);

    const lanData = {
        language : lang
    }
    let encryptLanData = encryptData(JSON.stringify(lanData));

    const [
        info,
        setInfo
    ] = useState();

    useEffect(() => {
        const getData = async () => {
            const response = await fetchPost(apiUrl.contactInfo, {data: encryptLanData});
            
            if (response?.success && response?.data?.data) {
                const decContactData = JSON.parse(decryptData(response.data.data));
                setInfo(decContactData);
            }
        }
        getData();
    }, []);

    const onClose = () => {
        navigation.goBack();
    }

    const viberHandler = () => {
  
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={'#f6f6f6'} barStyle='dark-content' />
            <TouchableOpacity
                activeOpacity={0.8}
                style={styles.close}
                onPress={onClose}>
                <AntDesign
                    name='closecircleo'
                    size={hp(3)}
                    color='#ff9700' />
            </TouchableOpacity>
            <Image  
                source={require('../../assets/images/contactus.jpeg')}
                style={styles.img} />
            <View style={styles.topContent}>
                <TouchableOpacity 
                    style={styles.block}
                    onPress={()=> Linking.openURL(`tel:${info?.phone}`)}>
                    <Text style={styles.label}>
                        {local.callUs}
                    </Text>
                    <View style={styles.iconRow}>
                        <FontAwesome
                            name='phone'
                            size={hp(2)}
                            color='rgba(255, 153, 0, 0.8)' />
                        <Text style={styles.value}>
                            {info?.phone}
                        </Text>
                    </View>
                </TouchableOpacity>
                {/* {agent_phone &&  <View style={styles.block}>
                    <Text style={styles.label}>
                        Call Agent
                    </Text>
                    <View style={styles.iconRow}>
                        <FontAwesome
                            name='phone'
                            size={hp(2)}
                            color='rgba(255, 153, 0, 0.8)' />
                        <Text style={styles.value}>
                            {agent_phone}
                        </Text>
                    </View>
                </View>} */}
                <TouchableOpacity
                    style={styles.block}
                    onPress={()=> Linking.openURL(`mailto:${info?.email}`)}>
                    <Text style={styles.label}>
                        {local.emailUs}
                    </Text>
                    <View style={styles.iconRow}>
                        <MaterialCommunityIcons
                            name='email'
                            size={hp(2)}
                            color='rgba(255, 153, 0, 0.8)' />
                        <Text style={styles.value}>
                            {info?.email}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
            {/* <View style={styles.orRow}>
                <View style={styles.left} />
                <Text style={styles.orTxt}>OR</Text>
                <View style={styles.left} />
            </View> */}
            {/* <View style={styles.socialRow}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.socialBtn} >
                    <Fontisto
                        name='viber'
                        size={hp(4)}
                        color='#ff9700' />
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.socialBtn} >
                    <MaterialCommunityIcons
                        name='facebook-messenger'
                        size={hp(4.7)}
                        color='#ff9700' />
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.socialBtn}
                    onPress={ viberHandler } >
                    <Fontisto
                        name='telegram'
                        size={hp(4)}
                        color='#ff9700' />
                </TouchableOpacity>
            </View> */}
        </View>
    )
}

export default ContactUs;