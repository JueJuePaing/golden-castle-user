import React from 'react';
import {
    View,
    Text,
    Image
} from 'react-native';
import Fontisto from "react-native-vector-icons/Fontisto";

import styles from './Style';
import { useLocal } from '../../hook/useLocal';
import Header from '../../components/header/DetailHeader';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

const NotificationDetail = ({navigation, route}) => {

    const local = useLocal();
    const {item, date} = route.params;

    return (
        <View>
            <Header 
                title={local.notification}
                goBack={()=> navigation.goBack()} />
            <View style={styles.detailContainer}>
                <Image
                    source={require('../../assets/images/noti.gif')}
                    style={ styles.noti } />
                <Text style={styles.detailTitle}>
                    { item.title }
                </Text>
                <Text style={styles.detailDesc}>
                    { item.message }
                </Text>
                <View style={styles.detailDateContent}>
                    <Fontisto 
                        name="date"
                        size={hp(1.7)}
                        color="#ff9700" />
                    <Text style={styles.detailDate}>
                        { `${item.time} ${date}` }
                    </Text>
                </View>
            </View>
        </View>
    )
}

export default NotificationDetail;