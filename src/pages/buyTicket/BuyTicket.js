import React, {
    useEffect,
    useContext,
    useState
} from 'react';
import {
    View,
    TouchableOpacity,
    Modal,
    Text,
    DeviceEventEmitter,
    ScrollView,
    ToastAndroid
} from 'react-native';
import {
    widthPercentageToDP as wp, 
    heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import Ionicons from "react-native-vector-icons/Ionicons";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

import { getItem } from '../../utils/appStorage';
import { fetchPostByToken } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { Context } from '../../context/Provider';
import { deleteMultiItems } from '../../utils/appStorage';
import { useLocal } from '../../hook/useLocal';
import { encryptData } from '../../utils/encryptData';
import { decryptData } from '../../utils/decryptData';

import styles from './Style';
import Header from '../../components/header/DetailHeader';
import TicketNumber from '../../components/ticketNumber/TicketNumber';
import NumKeyboard from '../../components/keyboard/NumKeyboard';
import Ticket from '../../components/ticket/Ticket';
import Loading from '../../components/loading/Loading';
import ErrorMessage from '../../components/errorMessage/ErrorMessage';
import LoginModal from '../auth/Login';

const BuyTicket = ({defaultTicket, onClose}) => {

    const {
        userInfo,
        lang,
        changeUserInfo,
        changeProfileInfo,
        net,
        profileInfo
    } = useContext(Context);

    const local = useLocal();

    const [
        tickets,
        setTickets
    ] = useState([""]);
    const [
        drawDate,
        setDrawDate
    ] = useState();
    const [
        loading,
        setLoading
    ] = useState(false);
    const [
        errMsg,
        setErrMsg
    ] = useState();
    const [
        showLogin,
        setShowLogin
    ] = useState(false);
    const [
        errorTickets,
        setErrorTickets
    ] = useState();
    const [
        exchangeRate,
        setExchangeRate
    ] = useState(null);
    const [
        profileData,
        setProfileData
    ] = useState();

    useEffect(() => {
        if (profileInfo) {
            setProfileData(profileInfo)
        } else {
            getProfile();
        }
    }, [profileInfo]);

    const getProfile = async () => {
        if (userInfo?.access_token) {
            setLoading(true);
            const lanData = {
                language : lang
            }
            let encryptLanData = encryptData(JSON.stringify(lanData), userInfo.secret_key);
            const response = await fetchPostByToken(apiUrl.profile, {data: encryptLanData}, userInfo.access_token);
            if (response?.success && response?.data) {
                
                const decUserInfo = JSON.parse(decryptData(response.data, userInfo.secret_key));
                setProfileData(decUserInfo.user);
                changeProfileInfo(decUserInfo.user);
            }
            else if (response?.status === 401) {
                logoutHandler();
            }
            setLoading(false);
        }
    }

    useEffect(() => {
        if (defaultTicket) {
            tickets[0] = defaultTicket;
            setTickets(tickets);
        }
    }, [defaultTicket]);

    useEffect(() => {
        const getExchangeRate = async () => {
            const exchange_rate = await getItem('@exchange_rate');
            if (exchange_rate) setExchangeRate(exchange_rate);
        }
        getExchangeRate();
    }, []);

    useEffect(() => {
        const getDrawDate = async () => {
            const date = await getItem('@nextdrawDate');
            setDrawDate(date);
        }
        getDrawDate();
    }, []);

    const addTicket = () => {
        tickets.push("");
        setTickets(prev => [...prev]);
    }

    const removeTicket = (index) => {
        tickets.splice(index, 1);
        setTickets(prev => [...prev]);
    }

    const purchaseHandler = async () => {

        if (!net) {
            ToastAndroid.show(local.noConnection, ToastAndroid.SHORT);
            return;
        }
        
        if (tickets.length === 0 || (tickets.length === 1 && tickets[0].length < 6)) {
            setErrMsg(local.invalidTicketNo);
            return;
        }

        if (userInfo && userInfo.access_token) {
            setLoading(true);

            let ticket_numbers = [];
            tickets.map((ticket) => {
                if (ticket && ticket.length === 6) {
                    ticket_numbers.push(ticket);
                }
            })

            let encryptPurchaseData = encryptData(JSON.stringify({ticket_numbers, language: lang}), userInfo.secret_key);

            const response = await fetchPostByToken(apiUrl.purchase, {data: encryptPurchaseData}, userInfo.access_token);

            if (response) {
                if (response?.success) {
                    await getProfile();
                    ToastAndroid.show(local.purchaseSuccessful, ToastAndroid.SHORT);
                    DeviceEventEmitter.emit('purchase_success', 'true');
                    DeviceEventEmitter.emit('noti_count_change', 'true')
                    clearData();
                } else {
                    if (response.status === 401) {
                        // Unauthenticated
                        logoutHandler();
                    } else if (response.message) {
                        setErrMsg(response.message); 
                        if (response.purchased_tickets && response.purchased_tickets.length > 0) {
                            setErrorTickets(response.purchased_tickets);
                        }  
                    } else {
                        setErrMsg(local.somethingWrong);   
                    }
                }
            } else {
                setLoading(false);
            }
        } else {
            setShowLogin(true);
        }
        setLoading(false);
    }

    const logoutHandler = async () => {
        await deleteMultiItems(['@userInfo']);
        changeUserInfo(null);
        changeProfileInfo();
        setShowLogin(true);
    }

    const clearData = () => {
        setTickets([""]);
        setErrMsg();
    }

    const pressKey = (num) => {
        setErrMsg(null);
        if (tickets[tickets.length-1].length < 6) {
            tickets[tickets.length-1] = tickets[tickets.length-1] + num;
            setTickets(prev => [...prev]);
        }
    }

    const pressBack = () => {
        if (tickets[tickets.length-1].length > 0) {
            let str = tickets[tickets.length-1].substring(0, tickets[tickets.length-1].length - 1);
            tickets[tickets.length-1] = str;
            setTickets(prev => [...prev]);
        }
    }

    return (
        <Modal
            transparent={false}
            visible={true}
            onRequestClose={onClose}
            animationType="fade">
            <View style={styles.modalView}>
                <Header
                    title={local.purchase}
                    goBack={onClose} 
                    exchange_rate={exchangeRate} />

                <Ticket
                    data={tickets}
                    buy={true}
                    drawDate={ drawDate }
                    live={ false } />
                    {/* // ticket_background={ticketBg}
                    // ticket_photo={ticketPhoto} /> */}

                <View style={ styles.pointRow }>
                    {/* <FontAwesome5
                        name="coins"
                        size={hp(1.6)}
                        color='#ff9700' /> */}
                    <Text style={styles.point}>
                        {`Point : ${profileData ? profileData.point : '0'}`}
                    </Text>
                </View> 
                <View style={styles.card}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                    {
                        tickets.map((ticket, index) => {
                        return <View key={index} style={styles.inputRow}>    
                            <TicketNumber number={ticket} width={wp(68)} />
                            {
                                index === tickets.length - 1 ?
                                <TouchableOpacity 
                                    activeOpacity={0.8}
                                    onPress={addTicket}
                                    disabled={tickets[tickets.length-1].length < 6}
                                    style={[styles.addBtn, {
                                        borderColor: tickets[tickets.length-1].length < 6 ? 'rgba(255, 151, 0, 0.3)' : '#ff9700'
                                    }]}>
                                    <Ionicons
                                        name="ios-add"
                                        size={hp(2.5)}
                                        color={tickets[tickets.length-1].length < 6 ? 'rgba(255, 151, 0, 0.3)' : '#ff9700'} />
                                </TouchableOpacity>
                                :
                                <TouchableOpacity 
                                    activeOpacity={0.8}
                                    onPress={()=> removeTicket(index)}
                                    style={styles.addBtn}>
                                    <Ionicons
                                        name="remove"
                                        size={hp(2.5)}
                                        color="#ff9700" />
                                </TouchableOpacity>
                            }
                        </View>
                        })
                    }
                    </ScrollView>
                </View>

                <ErrorMessage 
                    message={errMsg} 
                    width={wp(88)}
                    errorTickets={ errorTickets } />

                <TouchableOpacity 
                    activeOpacity={0.8}
                    onPress={purchaseHandler}
                    style={styles.submitBtn}>
                    <Text style={styles.submit}>{local.purchase}</Text>
                </TouchableOpacity>

                <NumKeyboard
                    pressKey={pressKey}
                    pressBack={pressBack}
                    marginBottom={50} />  
            </View>
            {loading && <Loading />}
            {
                showLogin && (
                    <LoginModal
                        onClose={()=> setShowLogin(false)}
                        defaultTab={ 1 } />
                )
            }
        </Modal>
    )
}

export default BuyTicket;