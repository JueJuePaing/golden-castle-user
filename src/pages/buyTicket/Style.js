import { StyleSheet } from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    modalView: {
        backgroundColor: '#f1f1f1',
        height: hp(100),
        width: wp(100)
    },
    card: {
        backgroundColor: '#fff',
        width: wp(90),
        padding: hp(2),
        alignSelf: 'center',
        marginTop: hp(2),
        borderRadius: hp(1),
        shadowOffset: {width: 5, height: 10},
        shadowColor: '#000',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3,
        maxHeight: hp(40),
        overflow: 'hidden'
    },
    submitBtn: {
        width: wp(90),
        height: hp(5),
        backgroundColor: '#ff9700',
        borderRadius: hp(3),
        flexDirection:'row',
        alignItems: 'center',
        marginTop: hp(3),
        alignSelf: 'center',
        justifyContent: 'center',
        shadowOffset: {width: 5, height: 10},
        shadowColor: '#000',
        shadowOpacity: 0.5,
        shadowRadius: hp(1),
        elevation: 3
    },
    submit: {
        fontSize :hp(1.6),
        color: '#fff',
        //paddingLeft: wp(4),
        fontFamily: 'Poppins-Medium',
        paddingTop: hp(.3)
    },
    inputRow: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    addBtn: {
        width: wp(8),
        height: wp(8),
        borderRadius: hp(1),
        borderWidth: 2,
        borderColor: '#ff9700',
        marginBottom: hp(.7),
        marginLeft: wp(4),
        justifyContent: 'center',
        alignItems: 'center'
    },
    pointRow: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'flex-end',
        marginRight: wp(3),
        paddingTop: hp(1)
    },
    point: {
        fontFamily: 'Poppins-Medium',
        fontSize: hp(1.6),
        color: '#000',
        paddingLeft: wp(1),
        paddingTop: hp(.5)
    }
})

export default styles;