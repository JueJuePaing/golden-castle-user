import React, {
    useState,
    useEffect,
    useContext,
    useCallback
} from 'react';
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    FlatList,
    ToastAndroid,
    StatusBar,
    RefreshControl
} from 'react-native';
import styles from './Style';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import AntDesign from "react-native-vector-icons/AntDesign";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import { fetchPost } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { Context } from '../../context/Provider';
import { formatNumber } from '../../utils/common';
import { useLocal } from '../../hook/useLocal';
import { encryptData } from '../../utils/encryptData';
import { decryptData } from '../../utils/decryptData';

import TicketNumber from '../../components/ticketNumber/TicketNumber';
import ResultDateModal from "../../components/modal/ResultDateModal";
import TicketSeparator from '../../components/separator/TicketSeparator';
import Loading from '../../components/loading/Loading';
import NoData from '../../components/noData/NoData';
import NetworkProblem from '../../components/modal/NetworkProblem';

const Result = ({navigation}) => {

    const { 
        lang,
        net
    } = useContext(Context);
    const local = useLocal();

    const lanData = {
        language : lang
    }
    let encryptLanData = encryptData(JSON.stringify(lanData));

    const [
        refreshing,
        setRefreshing
      ] = useState(false);
    const [
        results,
        setResults
    ] = useState();
    const [
        loading,
        setLoading
    ] = useState(true);
    const [
        drawDates,
        setDrawDates
    ] = useState([]);
    const [
        selectedDate,
        setSelectedDate
    ] = useState();
    const [
        showModal,
        setShowModal
    ] = useState(false);
    const [
        selected,
        setSelected
    ] = useState(2);
    const [
        winningNo,
        setWinningNo
    ] = useState('')
    const [
        noData,
        setNoData
    ] = useState(false);
    const [
        toggleSecond,
        setToggleSecond
    ] = useState(false);
    const [
        toggleThird,
        setToggleThird
    ] = useState(false);
    const [
        toggleFourth,
        setToggleFourth
    ] = useState(false);
    const [
        toggleFifth,
        setToggleFifth
    ] = useState(false);
    const [
        togglelastThree,
        setToggleLastThree
    ] = useState(false);
    const [
        toggleFirstThree,
        setToggleFirstThree
    ] = useState(false);
    const [
        togglelastTwo,
        setToggleLastTwo
    ] = useState(false);


    useEffect(() => {
       if (net) {
            setLoading(true);
            getDrawDates();
       }
    }, [net]);

    const onRefresh = useCallback(() => {
    
        const reloadData = async () => {
            setRefreshing(true);
            getDrawDates();
        };
    
        reloadData();
      }, []);


    const getDrawDates = async () => {
        const response = await fetchPost(apiUrl.getDrawDates, {data: encryptLanData});
        if (response?.data?.data) {
            const decDrawDates = JSON.parse(decryptData(response.data.data));
            if (decDrawDates?.draw_ids) { 
                setSelectedDate(decDrawDates.draw_ids[0]);
                setDrawDates(decDrawDates.draw_ids);
                getLotteryResults(decDrawDates.draw_ids[0]);
            } else {
                setLoading(false);
                setNoData(true);
            }
          } else {
            if (response?.status === 429) {
                ToastAndroid.show(local.tooManyAttempts, ToastAndroid.SHORT);
                if (drawDates?.length < 1) {
                    setNoData(true);
                }
            }
            setLoading(false)
          }
          setRefreshing(false);
    }

    const dateHandler = val => {
        setShowModal(false);
        setLoading(true);
        setSelectedDate(val);
        getLotteryResults(val);
    }

    const getLotteryResults = async (draw_id) => {
        const data = {
            draw_id,
            language: lang
        }
        let encryptResultData = encryptData(JSON.stringify(data));
        const response = await fetchPost(apiUrl.getLotteryResults, {data: encryptResultData});
        if (response?.success && response?.data?.data) {
            const decResults = JSON.parse(decryptData(response.data.data));

            if (decResults?.results) {
                setResults(decResults.results);
                setWinningNo((decResults.results.first.numbers[0]).toString());
                setNoData(false);
            } else {
                setNoData(true);
            }
        }
        setLoading(false);
    }

    return (
        <View style={styles.container}>
            <StatusBar barStyle={"light-content"} backgroundColor='#11212f' />

            <View style={styles.header}>
                <View>
                    <Text style={styles.title}>
                        {local.results}
                    </Text>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={styles.dateBtn}
                            disabled={!net}
                            onPress={()=> setShowModal(true)}>
                        <Text style={styles.date}>
                            { selectedDate ? selectedDate : 'N/A' }
                        </Text>
                        <AntDesign
                            name="caretdown"
                            size={hp(1.3)}
                            color="#ff9700" />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.checkBtn}
                    disabled={!net}
                    onPress={()=> navigation.navigate("CheckResult", {drawDates, date: selectedDate})}>
                    <Text style={styles.check}>
                        {local.checkResult}
                    </Text>
                    <MaterialCommunityIcons
                        name="note-search-outline"
                        size={hp(1.8)}
                        color="#fff" />
                </TouchableOpacity>
            </View>
        {
            !net ?
                <NetworkProblem /> :
                <>
                

        {
            !loading && noData && <NoData 
                message="NO DATA"
                reloadHandler={ () => {
                    setLoading(true);
                    getDrawDates();
                } } />
        }

        {
            !loading && !noData && (
                <>
                {
            results && (
                <ScrollView 
                    refreshControl={
                        <RefreshControl
                            refreshing={ refreshing }
                            colors={[ "#ff9700" ]}
                            onRefresh={ onRefresh }
                            progressViewOffset={hp(4)} />
                    }
                    showsVerticalScrollIndicator={false}>

                    <Text style={styles.test}>
                        {`${formatNumber(results.first.amount)} ${"\u0E3F"}`}
                    </Text>
                    <View style={styles.firstRow}>
                        <TouchableOpacity 
                            style={styles.beforeBlock}
                            activeOpacity={0.8}
                            onPress={()=> {
                                setSelected(1);
                                setWinningNo(results.closest_first.numbers[0].toString());
                            }}>
                            {selected === 1 && <View style={styles.smallSelectedBlock} />}
                            {selected === 1 && <View style={styles.smallSelectedDot} />}
                            <Text style={styles.beforeLabel}>
                                DOWN
                            </Text>
                            <Text style={styles.beforePrize}>
                                {`${formatNumber(results.closest_first.amount)} ${"\u0E3F"}`}
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            style={styles.firstBlock}
                            onPress={()=> {
                                setSelected(2);
                                setWinningNo(results.first.numbers[0].toString());
                            }}
                            activeOpacity={0.8}>
                            {selected === 2 && <View style={styles.selectedBlock} />}
                            {selected === 2 && <View style={styles.selectedDot} />}
                            <View style={styles.firstLabelContent}>
                                <Text style={styles.firstLabel}>
                                    1
                                </Text>
                                <Text style={styles.firstLabel2}>
                                    st
                                </Text>
                            </View>
                            <FontAwesome5
                                name="crown"
                                size={hp(3)}
                                color='#ff9700' />
                        </TouchableOpacity>
                        <TouchableOpacity 
                            style={[styles.beforeBlock, {borderTopLeftRadius: 0, borderTopRightRadius: hp(.7)}]}
                            onPress={()=> {
                                setSelected(3);
                                setWinningNo(results.closest_first.numbers[1].toString());
                            }}
                            activeOpacity={0.8}>
                            {selected === 3 && <View style={styles.smallSelectedBlock} />}
                            {selected === 3 && <View style={styles.smallSelectedDot} />}
                            <Text style={styles.beforeLabel}>
                                UP
                            </Text>
                            <Text style={styles.beforePrize}>
                                {`${formatNumber(results.closest_first.amount)} ${"\u0E3F"}`}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.winningNumContent}>
                        <View style={selected === 2 ? styles.winningNumBlock : styles.winningNumBlockOrange}>
                            <Text style={styles.winningNum}>
                                {winningNo.charAt(0)}
                            </Text>
                        </View>
                        <View style={selected === 2 ? styles.winningNumBlock : styles.winningNumBlockOrange}>
                            <Text style={styles.winningNum}>
                                {winningNo.charAt(1)}
                            </Text>
                        </View>
                        <View style={selected === 2 ? styles.winningNumBlock : styles.winningNumBlockOrange}>
                            <Text style={styles.winningNum}>
                            {winningNo.charAt(2)}
                            </Text>
                        </View>
                        <View style={selected === 2 ? styles.winningNumBlock : styles.winningNumBlockOrange}>
                            <Text style={styles.winningNum}>
                            {winningNo.charAt(3)}
                            </Text>
                        </View>
                        <View style={selected === 2 ? styles.winningNumBlock : styles.winningNumBlockOrange}>
                            <Text style={styles.winningNum}>
                            {winningNo.charAt(4)}
                            </Text>
                        </View>
                        <View style={selected === 2 ? styles.winningNumBlock : styles.winningNumBlockOrange}>
                            <Text style={styles.winningNum}>
                            {winningNo.charAt(5)}
                            </Text>
                        </View>
                    </View>

                    <View style={styles.block}>
                        <TouchableOpacity 
                            style={styles.blockHeader}
                            activeOpacity={0.8}
                            onPress={() => setToggleSecond(prev => !prev)}>
                            <Text style={styles.prizeHeader}>
                                {local.secondPrize}
                            </Text>
                            <View style={ styles.toggleRow }>
                                <Text style={styles.prizeAmount}>
                                    {`${formatNumber(results.second.amount)} ${"\u0E3F"}`}
                                </Text>
                                <View style={ styles.toggleBtn }>
                                    <AntDesign
                                        name={toggleSecond ? "caretup" : "caretdown"}
                                        size={hp(1.2)}
                                        color="#11212f"
                                        />
                                </View>
                            </View>
                        </TouchableOpacity>

                        {
                            toggleSecond && (
                                <>
                                    <TicketSeparator toggle={true} />
                                    {
                                        results.second.numbers.map((prize, index) => {
                                            return <TicketNumber key={index} number={prize.toString()} />
                                        })
                                    }
                                </>
                            )
                        }
                    
                    </View>

                    <View style={styles.block}>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={ styles.blockHeader }
                            onPress={() => setToggleThird(prev => !prev)}>
                            <Text style={styles.prizeHeader}>
                               {local.thirdPrize}
                            </Text>
                            <View style={ styles.toggleRow }>
                                <Text style={styles.prizeAmount}>
                                    {`${formatNumber(results.third.amount)} ${"\u0E3F"}`}
                                </Text>
                                <View style={ styles.toggleBtn }>
                                    <AntDesign
                                        name={toggleThird ? "caretup" : "caretdown"}
                                        size={hp(1.2)}
                                        color="#11212f"
                                        />
                                </View>
                            </View>
                        </TouchableOpacity>
                        {
                            toggleThird && (
                                <>
                                    <TicketSeparator toggle={true} />
                                    {
                                        results.third.numbers.map((prize, index) => {
                                            return <TicketNumber key={index} number={prize.toString()} />
                                        })
                                    }
                                </>
                            )
                        }
                    </View>

                    <View style={styles.block}>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={ styles.blockHeader }
                            onPress={() => setToggleFourth(prev => !prev)}>
                            <Text style={styles.prizeHeader}>
                                {local.fourthPrize}
                            </Text>
                            <View style={ styles.toggleRow }>
                                <Text style={styles.prizeAmount}>
                                    {`${formatNumber(results.forth.amount)} ${"\u0E3F"}`}
                                </Text>
                                <View style={ styles.toggleBtn }>
                                    <AntDesign
                                        name={toggleFourth ? "caretup" : "caretdown"}
                                        size={hp(1.2)}
                                        color="#11212f"
                                        />
                                </View>
                            </View>
                        </TouchableOpacity>
                        {
                            toggleFourth && (
                                <>
                                    <TicketSeparator />
                                    <FlatList
                                        showsVerticalScrollIndicator={false}
                                        numColumns={5}
                                        columnWrapperStyle={ styles.flatlist }
                                        keyExtractor={(item, index) => index.toString()}
                                        style={ styles.menu_list }
                                        data={ results.forth.numbers }
                                        renderItem={({item, index}) => (
                                            <View
                                                style={[styles.grid,
                                                    {
                                                        // borderBottomWidth: (index === 45 || index === 46 || index === 47 || index === 48 || index === 49) ? 0 : .2
                                                    }
                                                ]}>
                                                    <Text style={styles.gridNum}>
                                                        { item }
                                                    </Text>
                                            </View> 
                                        
                                        )}/>
                                </>
                            )
                        }
                    </View>

                    <View style={styles.block}>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={styles.blockHeader}
                            onPress={() => setToggleFifth(prev => !prev)}>
                            <Text style={styles.prizeHeader}>
                                {local.fifthPrize}
                            </Text>
                            <View style={ styles.toggleRow }>
                                <Text style={styles.prizeAmount}>
                                    {`${formatNumber(results.fifth.amount)} ${"\u0E3F"}`}
                                </Text>
                                <View style={ styles.toggleBtn }>
                                    <AntDesign
                                        name={toggleFifth ? "caretup" : "caretdown"}
                                        size={hp(1.2)}
                                        color="#11212f"
                                        />
                                </View>
                            </View>
                        </TouchableOpacity>
                        {
                            toggleFifth && (
                                <>
                                    <TicketSeparator toggle={true} />
                                    <FlatList
                                        showsVerticalScrollIndicator={false}
                                        numColumns={5}
                                        columnWrapperStyle={ styles.flatlist }
                                        keyExtractor={(item, index) => index.toString()}
                                        style={ styles.menu_list }
                                        data={ results.fifth.numbers }
                                        renderItem={({item, index}) => (
                                            <View style={styles.grid}>
                                                <Text style={styles.gridNum}>
                                                    { item }
                                                </Text>
                                            </View>
                                        )}/>
                                </>
                            )
                        }
                    </View>

                    <View style={styles.block}>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={ styles.blockHeader }
                            onPress={() => setToggleFirstThree(prev => !prev)}>
                            <Text style={styles.prizeHeader}>
                                {local.first3Digits}
                            </Text>
                            <View style={ styles.toggleRow }>
                                <Text style={styles.prizeAmount}>
                                    {`${formatNumber(results.first_three.amount)} ${"\u0E3F"}`}
                                </Text>
                                <View style={ styles.toggleBtn }>
                                    <AntDesign
                                        name={toggleFirstThree ? "caretup" : "caretdown"}
                                        size={hp(1.2)}
                                        color="#11212f"
                                        />
                                </View>
                            </View>
                        </TouchableOpacity>
                        {
                            toggleFirstThree && (
                                <>
                                    <TicketSeparator toggle={true} />
                                    {
                                        results.first_three.numbers.map((prize, index) => {
                                            return <TicketNumber key={index} number={prize.toString()} />
                                        })
                                    }
                                </>
                            )
                        }
                    </View>

                    <View style={styles.block}>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={ styles.blockHeader }
                            onPress={() => setToggleLastThree(prev => !prev)}>
                            <Text style={styles.prizeHeader}>
                                {local.last3Digits}
                            </Text>
                            <View style={ styles.toggleRow }>
                                <Text style={styles.prizeAmount}>
                                    {`${formatNumber(results.last_three.amount)} ${"\u0E3F"}`}
                                </Text>
                                <View style={ styles.toggleBtn }>
                                    <AntDesign
                                        name={togglelastThree ? "caretup" : "caretdown"}
                                        size={hp(1.2)}
                                        color="#11212f"
                                        />
                                </View>
                            </View>
                        </TouchableOpacity>
                        {
                            togglelastThree && (
                                <>
                                    <TicketSeparator toggle={true} />
                                    {
                                        results.last_three.numbers.map((prize, index) => {
                                            return <TicketNumber key={index} number={`---${prize.toString()}`} /> 
                                        })
                                    }
                                </>
                            )
                        }
                    </View>

                    <View style={[styles.block, {marginBottom: hp(10)}]}>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={ styles.blockHeader }
                            onPress={() => setToggleLastTwo(prev => !prev)}>
                            <Text style={styles.prizeHeader}>
                                {local.last2Digits}
                            </Text>
                            <View style={ styles.toggleRow }>
                                <Text style={styles.prizeAmount}>
                                    {`${formatNumber(results.last_two.amount)} ${"\u0E3F"}`}
                                </Text>
                                <View style={ styles.toggleBtn }>
                                    <AntDesign
                                        name={togglelastTwo ? "caretup" : "caretdown"}
                                        size={hp(1.2)}
                                        color="#11212f"
                                        />
                                </View>
                            </View>
                        </TouchableOpacity>
                        {
                            togglelastTwo && (
                                <>
                                    <TicketSeparator toggle={true} />
                                    <TicketNumber number={`----${results.last_two.numbers[0].toString()}`} />
                                </>
                            )
                        }
                    </View>
                </ScrollView>
            )
        }
                </>
            )
        }

        {showModal && <ResultDateModal
            drawDates={ drawDates }
            closeModalHandler={()=> setShowModal(false)}
            dateHandler={(val) => dateHandler(val)} />}
        {loading && <Loading />}

        </>
        }
        </View>
    )
}

export default Result;