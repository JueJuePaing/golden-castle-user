import React, { useState } from "react";
import { StatusBar } from "react-native";
import { SafeAreaProvider } from "react-native-safe-area-context";

const Context = React.createContext();

const Provider = (props) => {
    const { children } = props;
    const [auth, setAuth] = useState(true);
    const [lang, setLang] = useState('en');
    const [userInfo, setUserInfo] = useState(null);
    const [bannersData, setBanners] = useState();
    const [countdownData, setCountdownData] = useState();
    const [liveSaleData, setLiveSaleData] = useState();
    const [lastDrawData, setLastDrawData] = useState();
    const [profileInfo, setProfileInfo] = useState();
    const [net, setNet] = useState(true);
    const [wonLottery, setWonLottery] = useState(false);

    const context = {
        auth,
        lang,
        userInfo,
        bannersData,
        countdownData,
        liveSaleData,
        lastDrawData,
        profileInfo,
        wonLottery,
        net,
        changeAuth: val => {
          setAuth(val);
        },
        changeLang: val => {
          setLang(val);
        },
        changeUserInfo: val => {
          setUserInfo(val);
        },
        changeBanners: val => {
          setBanners(val);
        },
        changeCountdownData: val => {
          setCountdownData(val);
        },
        changeLiveSaleData: val => {
          setLiveSaleData(val);
        },
        changeLastDrawData: val => {
          setLastDrawData(val);
        },
        changeProfileInfo: val => {
          setProfileInfo(val);
        },
        changeNet: val => {
          setNet(val)
        },
        changeWonLottery: val => {
          setWonLottery(val)
        }
      };

    return (
      <Context.Provider value={context}>
        <SafeAreaProvider>
          <StatusBar style="dark" barStyle={'dark-content'} translucent backgroundColor="transparent" />
            {/* <StatusBar barStyle={'dark-content'} backgroundColor="#f1f1f1" /> */}
            {children}
        </SafeAreaProvider>
      </Context.Provider>
    );
};

export {
  Provider, Context
}
  