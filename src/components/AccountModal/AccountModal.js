import React from 'react';
import {
    View,
    TouchableOpacity,
    Text,
    Image,
    Modal
} from 'react-native';
import Fontisto from "react-native-vector-icons/Fontisto";
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

// Components
import styles from './style';

export default function AccountModal({
    recharge,
    selectedAccount,
    accounts,
    closeModalHandler,
    chooseHandler
}) {

    return (
        <Modal
            transparent={true}
            visible={true}
            onRequestClose={closeModalHandler}
            animationType="fade">
            <TouchableOpacity activeOpacity={1} onPressOut={closeModalHandler}>
                <View style={styles.modalView}>
                    <View style={styles.modalBg}>
                        {
                            accounts.map((account, index) => {
                                return (
                                    <TouchableOpacity
                                        activeOpacity={0.8}
                                        onPress={()=> chooseHandler(account, index)}
                                        style={[
                                            styles.item,
                                            index === 0 ? styles.first : index === accounts.length - 1 ? styles.last : null,
                                            {
                                                backgroundColor: ((recharge && account.id === selectedAccount.id) || (!recharge && index === selectedAccount.id)) ? "#f5f2f2" : '#fff',
                                            }]}>
                                        <View style={styles.row}>
                                            <Image
                                                source={{ uri : account.icon }}
                                                style={styles.flag} />
                                            <Text style={styles.name}>{ account.name }</Text>
                                        </View>
                                       <Fontisto
                                            name={((recharge && account.id === selectedAccount.id) || (!recharge && index === selectedAccount.id)) ? "radio-btn-active" : "radio-btn-passive"}
                                            size={hp(2.3)}
                                            color='#11212f' />
                                    </TouchableOpacity>
                                )
                            })
                        }
                    </View>
                </View>
            </TouchableOpacity>
        </Modal>
    )
}