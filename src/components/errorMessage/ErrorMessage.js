import React from 'react';
import styles from './style';
import {View, Text} from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { useLocal } from '../../hook/useLocal';

export default ErrorMessage = ({message, width=wp(75), errorTickets}) => {
    const local = useLocal();
    if (message)
        return (
            <View style={[styles.container, {width}]}>
                <Text style={styles.message}>
                    {message}
                </Text>
                {errorTickets && (
                    <>
                        <Text style={styles.title}>
                            {local.duplicateTickets}
                        </Text>
                        {errorTickets.map((ticket, index) => {
                            return <Text key={index} style={styles.ticket}>
                                {`- ${ticket}`}
                            </Text>
                        })}
                    </>
                )}
            </View>
        );
    else return null;
};
