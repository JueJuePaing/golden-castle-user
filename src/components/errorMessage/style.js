import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
  },
  message: {
    fontFamily: 'Poppins-Regular',
    fontSize: hp(1.4),
    color: '#d45148',
    paddingTop: hp(.5),
    marginBottom: hp(-1)
  },
  title: {
    fontFamily: 'Poppins-Medium',
    fontSize: hp(1.5),
    paddingTop: hp(2),
    color: '#11212f'
  },
  ticket: {
    fontFamily: 'Poppins-Medium',
    fontSize: hp(1.5),
    color: '#d45148',
  }
});

