import React from 'react';
import {
    View, 
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import AntDesign from "react-native-vector-icons/AntDesign"
import FontAwesome from "react-native-vector-icons/FontAwesome"
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

// Components
import styles from './style';
import Logo from '../../assets/icons/Logo';
import { useLocal } from '../../hook/useLocal';

export default function Header({loggedIn, name, lanHandler, callCenterHandler}) {
    const local = useLocal();
    return (
        <View style={styles.container}>
            <View style={styles.buttons}>
                {
                    loggedIn ?
                    <FontAwesome
                        name="user-circle-o"
                        size={wp(8)}
                        color='#ff9700'
                        style={styles.logo} /> :
                        <Image 
                            source={require('../../assets/images/logo_only.png')}
                            style={[styles.logoProfile, !loggedIn && styles.largeLogo]}/>
                    // <Logo />
                }
                <Text style={styles.title}>
                    {loggedIn ? `Hi ${name}` : ''}
                </Text>
            </View>
            <View style={styles.buttons}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={callCenterHandler}>
                    <AntDesign
                        name="customerservice"
                        size={22}
                        color='#fff'
                        />
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={lanHandler}>
                    <FontAwesome
                        name="language"
                        size={22}
                        color='#fff'
                        style={{
                            marginLeft: 10
                        }}
                        />
                </TouchableOpacity>
            </View>

        </View>
    )
}