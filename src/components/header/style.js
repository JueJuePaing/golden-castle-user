import {StyleSheet, StatusBar} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default styles = StyleSheet.create({
  container : {
    height: hp(6),
    width: wp(100),
    paddingHorizontal: wp(3),
    backgroundColor: '#11212f',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: StatusBar.currentHeight,
    paddingTop: hp(1)
  },
  buttons : {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  logo : {
    // width: wp(10), //12
    // height: wp(10),
    // resizeMode: 'contain',
    // borderRadius: wp(6)
  },
  title: {
    fontSize : hp(1.9),
    paddingLeft: wp(2),
    fontFamily: 'Poppins-Medium',
    color: '#fff'
  },
  logoProfile: {
    width : hp(5),
    height: hp(5),
    resizeMode: 'contain'
  },
  largeLogo: {
    width : hp(7),
    height: hp(5),
  },
  detailHeader: {
    backgroundColor: '#11212f',
    height: hp(6.5),
    width: wp(100),
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: wp(3),
    borderBottomLeftRadius: hp(2),
    borderBottomRightRadius: hp(2),
    alignItems: 'center'
  },
  leftRow: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  detailTitle: {
    fontSize : hp(1.8),
    fontFamily: 'Poppins-Medium',
    color: '#fff',
    paddingTop: hp(.3),
    paddingLeft: wp(2)
  },
  dateBtn: {
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: hp(.5)
},
date: {
    fontFamily: 'Poppins-Regular',
    fontSize: hp(1.6),
    color: '#ff9700',
    paddingRight: wp(2),
    paddingTop: hp(.35)
},
currencyRow: {
  
},
currency: {
  fontFamily: 'Poppins-Regular',
  fontSize: hp(1.6),
  color: '#ff9700',
  paddingRight: wp(2),
  paddingTop: hp(.35)
},
header: {
  backgroundColor: '#11212f',
  height: hp(5.5),
  width: wp(100),
  alignItems: 'center',
  justifyContent: 'center',
  marginTop : StatusBar.currentHeight,
  borderBottomLeftRadius: hp(2),
  borderBottomRightRadius: hp(2),
},
});
