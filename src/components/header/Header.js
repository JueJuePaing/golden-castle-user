import React from 'react';
import {
    View, 
    Image,
    Text,
    StatusBar
} from 'react-native';
// Components
import styles from './style';

export default function Header({ title }) {

    return (
        <View style={styles.header}>
            <Text style={styles.detailTitle}>
                { title }
            </Text>
        </View>
    )
}