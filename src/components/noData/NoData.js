import React from 'react';
import styles from './style';
import {View, TouchableOpacity, Text} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default NoData = ({ message, reloadHandler }) => {
  return (
    <View style={ styles.reloadContent }>
      <Text style={ styles.nodata }>{message}</Text>
      {reloadHandler && <TouchableOpacity
          activeOpacity={0.8}
          style={styles.reloadBtn}
          onPress={ reloadHandler }>
          <Ionicons
              name='reload'
              size={25}
              />
      </TouchableOpacity>}
    </View>
  );
};
