
import React, { useEffect } from 'react';
import { View } from 'react-native';
import Animated, {
  useSharedValue,
  useAnimatedStyle,
  withTiming,
  Easing,
} from 'react-native-reanimated';
import styles from './style';

const LoadingIndicator = () => {
  const circle1Position = useSharedValue(0);
  const circle2Position = useSharedValue(0);
  const circle1Opacity = useSharedValue(1);
  const circle2Opacity = useSharedValue(1);

  useEffect(() => {
    const swapPositions = () => {
      circle1Position.value = withTiming(
        circle1Position.value === 0 ? 23 : 0,
        { duration: 600, easing: Easing.linear }
      );
      circle1Opacity.value = withTiming(
        circle1Opacity.value === 1 ? 0 : 1,
        { duration: 600, easing: Easing.linear }
      );
      circle2Position.value = withTiming(
        circle2Position.value === 0 ? -23 : 0,
        { duration: 600, easing: Easing.linear }
      );
      circle2Opacity.value = withTiming(
        circle2Opacity.value === 1 ? 0 : 1,
        { duration: 600, easing: Easing.linear }
      );
    };

    swapPositions();
    const interval = setInterval(swapPositions, 700);

    return () => clearInterval(interval);
  }, [circle1Position, circle2Position]);

  const circle1Style = useAnimatedStyle(() => {
    return {
      transform: [{ 
        translateX: circle1Position.value
      }],
    };
  });

  const circle2Style = useAnimatedStyle(() => {
    return {
      transform: [{ 
        translateX: circle2Position.value
      }],
    };
  });

  return (
    <View style={[styles.loadingView, styles.container]}>
      <Animated.View style={[styles.circle, styles.redCircle, circle1Style]} />
      <Animated.View style={[styles.circle, styles.greenCircle, circle2Style]} />
    </View>
  );
};

export default LoadingIndicator;
