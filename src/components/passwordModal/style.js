import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from 'react-native-responsive-screen';

export default styles = StyleSheet.create({
  modalContainerMain: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'rgba(0,0,0, 0.8)',
    paddingTop: hp(15)
  },
  modalContainer: {
    width: wp(90),
    // height: hp(26),
    backgroundColor: '#FFF',
    alignSelf: 'center',
    paddingHorizontal: hp(1),
    borderRadius: hp(1),
    justifyContent: 'space-between',
  },
  modalTitle: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  close: {
    marginTop: hp(1),
    position: 'absolute',
    right: wp(1)
  },
  title: {
    fontFamily: 'Poppins-Medium',
    fontSize: hp(1.9),
    width: wp(80),
    marginVertical: hp(2),
    textAlign: 'center',
    color: '#64696D', //e1e6f5 64696D
  },
  // amountContent: {
  //   fontFamily: 'Poppins-Medium',
  //   flexDirection: 'row',
  //   width: '100%',
  //   borderTopColor: '#DDD',
  //   borderTopWidth: 0.5,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  // },
  transferText: {
    fontFamily: 'Poppins-Medium',
    fontSize: hp(1.7),
    marginTop: hp(1),
    color: '#64696D',
  },
  descContent: {
    marginTop: hp(2),
    paddingVertical: hp(1),
    paddingLeft: hp(1),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: '#DDD',
    borderTopWidth: 0.2,
    borderBottomWidth: 0.2,
    backgroundColor: '#FFF',
  },
  pointContent: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  point: {
    fontFamily: 'Poppins-Medium',
    fontSize: hp(1.6),
    marginTop: 6,
    color: '#64696D',
    marginLeft: wp(2)
  },
  currency: {
    fontFamily: 'Poppins-Regular',
    fontSize: hp(1.6),
    color: '#ff9700'
  },
  inputText: {
    fontFamily: 'Poppins-Regular',
    fontSize: hp(1.7),
    color: '#000',
    paddingVertical: 0,
    width: wp(82),
    borderWidth: 1,
    height: hp(4.8),
    borderRadius: hp(1),
    borderColor: 'lightgray',
    paddingHorizontal: wp(3),
    paddingTop: hp(.3),
    alignSelf: 'center',
    marginTop: hp(2)
  },
  footer: {
    alignSelf: 'center',
     flexDirection: 'row',
     marginTop: hp(3),
     marginBottom: hp(1.5)
 },
 confirmBtn: {
     height: hp(4.2),
     alignItems: 'center',
     justifyContent: 'center',
     backgroundColor: '#11212F',
     borderRadius: hp(1),
     borderWidth: 1,
     borderColor: '#FF9700',

     shadowOffset: {width: 5, height: 10},
     shadowColor: '#fff',
     shadowOpacity: 0.5,
     shadowRadius: hp(1),
     elevation: 3,
     paddingHorizontal: wp(6)
     //width: wp(35)
 },
 confirmTxt: {
     fontFamily: 'Poppins-Medium',
     fontSize: hp(1.6),
     color: '#fff',
     paddingTop: hp(.5),
     textTransform: 'uppercase'
 },
 cancelBtn: {
     height: hp(4.2),
     alignItems: 'center',
     justifyContent: 'center',
     borderRadius: hp(1),
    // borderWidth: 0.7,
     borderColor: '#11212f',

     shadowOffset: {width: 5, height: 10},
     shadowColor: '#fff',
     shadowOpacity: 0.5,
     shadowRadius: hp(1),
     elevation: 3,
     //paddingHorizontal: wp(2),
     marginRight: wp(3)
    //  width: wp(35)
 },
 cancelTxt: {
     fontFamily: 'Poppins-Regular',
     fontSize: hp(1.6),
     color: '#11212f',
     paddingTop: hp(.5),
     textTransform: 'uppercase'
 },
});
