import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default styles = StyleSheet.create({
  titleContent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  small: {
      backgroundColor: 'darkgrey',
      width: 2,
      height: 1,
      borderRadius: 5
  },
  big: {
      backgroundColor: 'darkgrey',
      width: 15,
      height: 1,
      borderRadius: 10,
      marginLeft: 2
  },
  title: {
      fontSize :hp(1.5),
      color:'darkgrey',
      fontFamily: 'Poppins-Medium',
      paddingHorizontal: 10,
      paddingTop: 1
  },
});
