import React from 'react';
import {View, Image} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import {
  widthPercentageToDP as wp
} from 'react-native-responsive-screen';

// Components
import styles from './style';

const SLIDER_1_FIRST_ITEM = 0;

export default function slideImage({data, height}) {

  const _renderItem = ({item}) => {
    return (
          <Image 
            source={{ uri : item.url }} 
            style={[styles.renderImage, {height : item.height}]} />
    );
  };



  return (
    <>
      <View style={[styles.container, {height : height}]}>
          <Carousel
            layout={'default'}
            loop={true}
            autoplay={true}
            autoplayDelay={4000}
            autoplayInterval={8000}
            activeAnimationType={'timing'}
            firstItem={SLIDER_1_FIRST_ITEM}
            inactiveSlideScale={0.9}
            inactiveSlideOpacity={0.7}
            inactiveSlideShift={0.6}
            sliderWidth={wp(100)}
            itemWidth={wp(92)}
            pagingEnabled={true}
            loopClonesPerSide={data.length}
            data={data}
            renderItem={_renderItem}
          />
      </View>
    </>
  );
}
