import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default styles = StyleSheet.create({
  container: {
    width: wp(90),
    borderRadius: hp(.6),
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: hp(.5),
    alignSelf: 'center'
  },
  renderImageContainer: {
    width : wp(92),
   //  justifyContent: 'center',
    // alignItems: 'center',
    // borderRadius: hp(0.6), // Adjust the radius as needed
    // overflow: 'hidden',
  },
  renderImage: {
    width : wp(92),
    resizeMode: 'contain',
    borderRadius: hp(0.6),
  },
  btn: {
    width: '100%',
    height: '100%',
    zIndex: 2,
    position: 'absolute',
  },
  ContainerStyle: {
    backgroundColor: 'transparent',
    paddingHorizontal: 0,
    paddingVertical: hp(1),
  },
  dotContainer: {
    marginHorizontal: hp(0.2),
  },
  dotStyle: {
    width: wp(3),
    height: hp(0.5),
    backgroundColor: '#11212f',
    margin: 0,
  },
  inDotStyle: {
    backgroundColor: '#11212f',
    width: hp(1),
    height: hp(1),
  },
  indicatorContent: {
    maxHeight: hp(10),
    width: wp(100),
  },
});
