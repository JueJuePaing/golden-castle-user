import React from 'react';
import { View } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

// Components
import styles from './style';

export default function TicketSeparator({width=wp(84)}) {

    return (
        <View style={[styles.separatorRow, {width}]}>
            <View style={styles.curve} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.separatorBlock} />
            <View style={styles.rightCurve} />
        </View>
    )
}