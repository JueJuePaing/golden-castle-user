import React from 'react';
import {
    View, 
    TouchableOpacity,
    Modal,
    Text
} from 'react-native';
// Components
import styles from './style';
import { useLocal } from '../../hook/useLocal';

export default function CustomAlert({
    message,
    closeHandler
 }) {
    const local = useLocal();
    return (
        <Modal
            transparent={true}
            visible={true}
            animationType="fade">
                <View style={styles.modalView}>
                    <View style={styles.sessionModalBg}>
                        <Text style={ styles.alertMessage }>
                            {message}
                        </Text>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={ styles.submitBtn }
                            onPress={ closeHandler }>
                            <Text style={styles.submitTxt}>{local.ok}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
        </Modal>
    )
}
