import React from 'react';
import {
    View, 
    TouchableOpacity,
    Modal,
    Text,
    ScrollView,
} from 'react-native';

// Components
import styles from './style';

export default function ResultDateModal({ 
    drawDates,
    closeModalHandler,
    dateHandler
 }) {

    return (
        <Modal
            transparent={true}
            visible={true}
            onRequestClose={closeModalHandler}
            animationType="fade">
            <TouchableOpacity activeOpacity={1} onPressOut={closeModalHandler}>
                <View style={styles.modalView}>
                    <View style={styles.modalBg}>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            {
                                drawDates.map((option, index) => {
                                    return (
                                        <TouchableOpacity
                                            onPress={()=> dateHandler(option)}
                                            activeOpacity={0.8}
                                            key={index.toString()}
                                            style={[styles.option, {
                                                borderBottomWidth : index === drawDates.length - 1 ? 0 : 0.5
                                            }]}>
                                            <Text style={styles.optionData}>
                                                {option}
                                            </Text>
                                        </TouchableOpacity>
                                    )
                                })
                            }
                        </ScrollView>
                    </View>
                </View>
            </TouchableOpacity>
        </Modal>
    )
}
