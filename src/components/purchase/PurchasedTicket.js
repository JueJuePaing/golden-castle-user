import React from 'react';
import styles from './style';
import {View, Text} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import Ionicons from 'react-native-vector-icons/Ionicons'

import Ticket from '../../assets/icons/Ticket';
import TicketSeparator from "../separator/TicketSeparator";

export default PurchasedTicket = ({
  day,
  month,
  tickets
}) => {

  return (
  <View style={[styles.content, {marginTop: 0}]}>
      <View style={styles.dateContent}>
          <Text style={styles.date}>
              {day}
          </Text>
          <Text style={styles.month}>
            {month}
          </Text>
      </View>
      <View style={styles.ticketContent}>
          {
              tickets.map((ticket,index) => {
                  return (
                      <View key={index} style={styles.ticketBlock}>
                          <View style={styles.ticket}>
                              <Ticket />
                              <View style={styles.ticketnoContent}>
                                  <Text style={styles.ticketNo}>
                                      { ticket.number }
                                  </Text>
                              </View>
                          </View>

                          <TicketSeparator width={wp(75)} />
                              
                          <View style={styles.ticketFooter}>
                              <View style={styles.row}>
                                  <Ionicons
                                      name='pricetags'
                                      size={hp(1.8)}
                                      color='#ff9700' />
                                  <Text style={styles.price}>
                                      {`${ticket.amount} ${"\u0E3F"}`}
                                  </Text>
                              </View>
                              <View style={styles.row}>
                                  <Ionicons
                                      name='time'
                                      size={hp(1.6)}
                                      color='gray' />
                                  <Text style={styles.ticketDate}>
                                      { ticket.purchased_date }
                                  </Text>
                              </View>
                          </View>
                      </View>
                  )
              })
          }
      </View>
  </View>
  );
};
