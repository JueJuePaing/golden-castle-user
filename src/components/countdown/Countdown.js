import React from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import Entypo from "react-native-vector-icons/Entypo";

// Components
import styles from './style';
import { useLocal } from '../../hook/useLocal';

export default function Countdown({
    countdownDate,
    countdownDay,
    countdownHour,
    countdownMin,
    countdownSec,
    buyHandler
}) {
    const local = useLocal();

    return (
        <View style={styles.countdownContent}>

            <View style={styles.countdown}>
                <View style={styles.date}>
                    {
                        countdownDate ? (
                            <>
                                <Text style={styles.day}>{ countdownDate[0] }</Text>
                                <Text style={styles.month}>{ countdownDate[1] }</Text>
                            </>
                        ) : <Text style={styles.month}>N/A</Text>
                    }
                </View> 

                <View style={styles.time}>
                    <Text style={styles.label}>
                        { local.nextdraw }
                    </Text>
                    <View style={styles.row}>
                        <View>
                            <View style={styles.col}>
                                <Text style={styles.data}>{ countdownDay }</Text>
                            </View>
                            <View style={styles.bottomCol}>
                                <Text style={styles.timeStatus}>D</Text>
                            </View>
                        </View>
                        <View style={styles.col2}>
                            <Text style={styles.semi}>:</Text>
                        </View>
                        <View>
                            <View style={styles.col}>
                                <Text style={styles.data}>{countdownHour}</Text>
                            </View>
                            <View style={styles.bottomCol}>
                                <Text style={styles.timeStatus}>H</Text>
                            </View>
                        </View>
                        <View style={styles.col2}>
                            <Text style={styles.semi}>:</Text>
                        </View>
                        <View>
                            <View style={styles.col}>
                                <Text style={styles.data}>{countdownMin}</Text>
                            </View>
                            <View style={styles.bottomCol}>
                                    <Text style={styles.timeStatus}>M</Text>
                                </View>
                        </View>
                        <View style={styles.col2}>
                            <Text style={styles.semi}>:</Text>
                        </View>
                        <View>
                            <View style={styles.col}>
                                <Text style={styles.data}>{countdownSec}</Text>
                            </View>
                            <View style={styles.bottomCol}>
                                <Text style={styles.timeStatus}>S</Text>
                            </View>
                        </View>
                    </View>

                </View>

                <TouchableOpacity 
                    activeOpacity={0.8}
                    style={styles.buyBtn}
                    onPress={ buyHandler }>
                    <Text style={styles.buy}>{local.buyTab}</Text>
                    <View style={styles.moreBtn}>
                        <Entypo
                            name="chevron-small-right"
                            size={13}
                            color="#fff" />
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}