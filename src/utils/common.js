export const formatNumber = (num) => {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

export const numArray = (from, to) => {
    var list = [];
    for (var i = from; i <= to; i++) {
        list.push(i);
    }
    return list;
}