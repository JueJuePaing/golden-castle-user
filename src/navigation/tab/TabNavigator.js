import React, {
  useState,
  useContext,
  useEffect,
  useCallback
} from 'react';
  import {
    Modal,
    DeviceEventEmitter,
    Animated,
    TouchableOpacity,
    Text,
    View,
    ScrollView,
    ToastAndroid,
    Image
  } from 'react-native';
import { CurvedBottomBar } from 'react-native-curved-bottom-bar';
import { NavigationContainer,  } from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import PushNotification from "react-native-push-notification";
import messaging from "@react-native-firebase/messaging";

import { Context } from '../../context/Provider';
import { formatNumber } from '../../utils/common';
import { fetchPostByToken } from '../../utils/fetchData';
import apiUrl from '../../utils/apiUrl';
import { useLocal } from '../../hook/useLocal';
import { encryptData } from '../../utils/encryptData';
import { decryptData } from '../../utils/decryptData';

import styles from './Style';
import TicketNumber from '../../components/ticketNumber/TicketNumber';
import HomeStackNavigator from '../stack/HomeStackNavigator';
import ResultStackNavigator from '../stack/ResultStackNavigator';
import BuyTicket from '../../pages/buyTicket/BuyTicket';
import NotificationStackNavigator from '../stack/NotificationStackNavigator';
import ProfileStackNavigator from '../stack/ProfileStackNavigator';

const TabBar = () => {

  const local = useLocal();
  const {
    userInfo,
    countdownData,
    lang
  } = useContext(Context);

  const lanData = {
    language : lang
  }
 
  const [
    profileData,
    setProfileData
  ] = useState();
  const [
    buying,
    setBuying
  ] = useState(false);
  const [
    showCongratulation,
    setShowCongratulation
  ] = useState(false);
  const [
    winning_tickets,
    setWinningTickets
  ] = useState([]);

  useEffect(()=> {
    const eventEmitter = DeviceEventEmitter.addListener(
        'noti_count_change',
        val => {
          if (val === "true") {
            getProfile();
          }
        },
      );
      getProfile();
      return () => eventEmitter;
}, [userInfo])

const getProfile = async () => {
    if (userInfo?.access_token) {
        let encryptLanData = encryptData(JSON.stringify(lanData), userInfo.secret_key);
        const response = await fetchPostByToken(apiUrl.profile, {data: encryptLanData}, userInfo.access_token);
        if (response?.success && response?.data) {
          const decUserInfo = JSON.parse(decryptData(response.data, userInfo.secret_key));
          setProfileData(decUserInfo.user);
        }
    } else {
      setProfileData();
    }
}

  useEffect(() => {

    PushNotification.configure({

      // (required) Called when a remote is received or opened, or local notification is opened
      onNotification: function (notification) {

        getProfile();
        if (notification && notification.data && notification.data.title && notification.data.message) {
          displayNotification(notification.data.title, notification.data.message);
        }
        if (notification && notification.userInteraction && notification.title === "Congratulations") {
          checkLotteryWin(userInfo);
        }
      }
    });

    messaging().setBackgroundMessageHandler(async remoteMessage => {
      console.log('setBackgroundMessageHandler : ' + JSON.stringify(remoteMessage));
    });
  }, [userInfo]);

  const checkLotteryWin = async (info) => {
    if (info && info.access_token) {
      let encryptLanData = encryptData(JSON.stringify(lanData), info.secret_key);
      const response = await fetchPostByToken(apiUrl.checkLotteryWon, {data: encryptLanData}, info.access_token);

      if (response?.success && response?.data) {
        const decLotteryWin = JSON.parse(decryptData(response.data, info.secret_key));
        if (decLotteryWin?.results?.length > 0) {
          setWinningTickets(decLotteryWin.results);
          setShowCongratulation(true);
        }
      }
    }
  }

const displayNotification = (title, desc) => {

  const options = {
    soundName: 'default',
    playSound: true
  }

  PushNotification.localNotification({

    channelId : "fcm_fallback_notification_channel",
    autoCancel: true,
    largeIcon: options.largeIcon || 'ic_launcher',
    smallIcon: options.smallIcon || 'ic_launcher',
    bigText: desc || '',
    subText: title || '',
    vibrate: options.vibrate || true,
    vibration: options.vibration || 300,
    priority: options.priority || 'high',
    importance: options.importance || 'high',
    
    title: title || '',
    message: desc || '',
    playSound: true,
    //soundName: 'default',
    userInteraction: true,
    invokeApp: true
  })
}

  const _renderIcon = (routeName, selectedTab) => {
    let icon = '';

    switch (routeName) {
      case 'Home1':
        icon = 'ios-home-outline';
        break;
      case 'ResultStack':
        icon = 'ios-trophy-outline';
        break;
        case 'title3':
            icon = 'notifications-outline';
            break;
        case 'Profile':
            icon = 'user';
            break;
    }

    return (
        <View style={{
          backgroundColor: routeName === selectedTab ? '#11212f' : '#fff',
          width: 35,
          height: 35,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 20}}>
          {
            icon === "user" ?
              <AntDesign
                name={icon}
                size={routeName === selectedTab ? hp(2.2) : hp(3)}
                color={routeName === selectedTab ? '#fff' : 'gray'} /> :
              <Ionicons
                name={icon}
                size={routeName === selectedTab ? hp(2.2) : hp(3)}
                color={routeName === selectedTab ? '#fff' : 'gray'}
                />
          }
          {
            routeName === "title3" && profileData && profileData.unread_noti_count !== 0 && profileData.unread_noti_count !== '0' && (
              <TouchableOpacity style={styles.badge}>
                <Text style={styles.notiCount}>
                  {profileData.unread_noti_count}
                </Text>
              </TouchableOpacity>
            )
        }
        </View>
      );

  };
  
  const renderTabBar = ({ routeName, selectedTab, navigate }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          navigate(routeName);
        }}
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        {_renderIcon(routeName, selectedTab)}
      </TouchableOpacity>
    );
  };

  const onSubmit = () => {
    setShowCongratulation(false);
  }

  if (showCongratulation)
  return (
    <Modal
            transparent={true}
            visible={true}
            animationType="fade">
            <Image
                source={require('../../assets/images/congratulation.jpg')}
                style={styles.conImg}/>
            <View style={styles.modalView}>

                <View style={styles.modalBg}>
                    <Image
                        source={require('../../assets/images/crown.png')}
                        style={styles.crown}/>

                    <ScrollView showsVerticalScrollIndicator={false}>
                        {
                            winning_tickets.map((winner, index) => {
                                return (
                                    <View
                                        key={index}
                                        style={styles.prizeRow}>
                                        <Text style={styles.prizeTitle}>
                                          {
                                                winner.type === "first" ? local.firstPrize :
                                                winner.type === "closest_first" ? local.closetFirstPrize :
                                                winner.type === "second" ? local.secondPrize :
                                                winner.type === "third" ? local.thirdPrize :
                                                winner.type === "forth" ? local.fourthPrize :
                                                winner.type === "fifth" ? local.fifthPrize : 
                                                winner.type === "first_three" ? local.first3Digits :
                                                winner.type === "last_three" ? local.last3Digits :
                                                winner.type === "last_two" ? local.last2Digits : ""
                                            }
                                        </Text>
                                        <Text style={styles.prizeAmt}>
                                            {`${formatNumber(winner.amount)} ${"\u0E3F"}`}
                                        </Text>
                                        {
                                            winner.tickets.map((ticket, index) => {
                                                return <TicketNumber key={index} number={ticket.toString()} color="#ff9700" />
                                            })
                                        }
                                    </View>
                                )
                            })
                        }
                    </ScrollView>
                </View>
                <TouchableOpacity 
                    activeOpacity={0.8}
                    onPress={onSubmit}
                    style={styles.submitBtn}>
                    <Text style={styles.submit}>{local.ok}</Text>
                </TouchableOpacity>
            </View>
        </Modal>
  )

  return (
    <View style={{ flex: 1 }}>
      <NavigationContainer>
        <CurvedBottomBar.Navigator
             screenOptions={{
                headerShown: false
            }}
          type="UP"
          style={styles.bottomBar}
          strokeWidth={0.5}
          strokeColor="#DDDDDD"
          height={55}
          circleWidth={55}
          bgColor="white"
          initialRouteName="Home1"
          borderTopLeftRight
          renderCircle={({ selectedTab, navigate }) => (
            <Animated.View style={styles.btnCircleUp}>
              <TouchableOpacity
                style={{
                  flex: 1,
                  justifyContent: 'center',
                }}
                onPress={() => {
                  if (countdownData.day_diff) {
                    setBuying(true)
                  } else {
                    ToastAndroid.show(local.cannotBuy, ToastAndroid.LONG)
                  }
                  
                }}>
                     <Text style={{
                        fontSize: hp(1.8),
                        fontFamily: 'Poppins-Medium',
                        color: '#fff',
                        paddingTop: 6,
                        textTransform: 'uppercase'
                    }}>{local.buyTab}</Text>
              </TouchableOpacity>
            </Animated.View>
          )}
          tabBar={renderTabBar}>
             <CurvedBottomBar.Screen
              name="Home1"
              position="LEFT"
              component={() => (
                <HomeStackNavigator />
              )}
            />
            <CurvedBottomBar.Screen
              name="ResultStack"
              position="LEFT"
            >
              {props => <ResultStackNavigator {...props}/>}
            </CurvedBottomBar.Screen>
             <CurvedBottomBar.Screen
              name="title3"
              position="RIGHT"
            >
               {props => <NotificationStackNavigator {...props}/>}
            </CurvedBottomBar.Screen>
            <CurvedBottomBar.Screen
              name="Profile"
              position="RIGHT"
            >
              {props => <ProfileStackNavigator {...props}/>}
            </CurvedBottomBar.Screen>

        </CurvedBottomBar.Navigator>
        {buying && <BuyTicket
          onClose={()=> setBuying(false)}
        />}
      </NavigationContainer>
    </View>
  );
};


export default TabBar;