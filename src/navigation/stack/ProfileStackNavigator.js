import React from 'react';
import { Easing } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

// Components
import ProfileScreen from '../../pages/profile/Profile';
import PurchasedTicketsScreen from '../../pages/purchasedTickets/PurchasedTickets';
import WinnerList from '../../pages/winnerList/WinnerList';
import ChangePassword from '../../pages/changePassword/ChangePassword';
import ContactUs from '../../pages/contactUs/ContactUs';
import AboutUs from '../../pages/aboutUs/AboutUs';
import FAQ from '../../pages/faq/FAQ';
import AccountDeletion from '../../pages/accountDeletion/AccountDeletion';
import Feedback from '../../pages/feedback/Feedback';
import RechargeScreen from '../../pages/recharge/Recharge';
import ReceiptScreen from '../../pages/recharge/Receipt';
import RechargeHistory from '../../pages/recharge/RechargeHistory';
import RecordDetail from '../../pages/recharge/RecordDetail';
import WithdrawScreen from '../../pages/withdraw/Withdraw';
import WithdrawHistory from '../../pages/withdraw/WithdrawHistory';
import WithdrawRecordDetail from '../../pages/withdraw/RecordDetail';
import EditProfile from '../../pages/editProfile/EditProfile';

const Stack = createStackNavigator();

const config = {
  animation: 'timing',
  config: {
    duration: 200,
    easing: Easing.linear,
  }
};

const ProfileStackNavigator = () => {

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureDirection: 'horizontal',
        gestureEnabled: false,
        transitionSpec: {
          open: config,
          close: config
        }
      }}>
      <Stack.Screen
        name="Profile"
        component={ProfileScreen} />
      <Stack.Screen
        name="PurchasedTickets"
        component={PurchasedTicketsScreen} />
      <Stack.Screen
        name="WinnerList"
        component={WinnerList} />
      <Stack.Screen
        name="ChangePassword"
        component={ChangePassword} />
      <Stack.Screen
        name="ContactUs"
        component={ContactUs} />
      <Stack.Screen
        name="FAQ"
        component={FAQ} />
      <Stack.Screen
        name="AccountDeletion"
        component={AccountDeletion} />
      <Stack.Screen
        name="Feedback"
        component={Feedback} />
      <Stack.Screen
        name="AboutUs"
        component={AboutUs} />
      <Stack.Screen
        name="Recharge"
        component={RechargeScreen} />
      <Stack.Screen
        name="Receipt"
        component={ReceiptScreen} />
      <Stack.Screen
        name="RechargeHistory"
        component={RechargeHistory} />
      <Stack.Screen
        name="RecordDetail"
        component={RecordDetail} />
      <Stack.Screen
        name="Withdraw"
        component={WithdrawScreen} />
      <Stack.Screen
        name="WithdrawHistory"
        component={WithdrawHistory} />
      <Stack.Screen
        name="WithdrawRecordDetail"
        component={WithdrawRecordDetail} />
      <Stack.Screen
        name="EditProfile"
        component={EditProfile} />
    </Stack.Navigator>
  );
};

export default ProfileStackNavigator;
