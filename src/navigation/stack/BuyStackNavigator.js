import React from 'react';
import { Easing } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

// Components
import BuyTicket from '../../pages/buyTicket/BuyTicket';

const Stack = createStackNavigator();

const config = {
  animation: 'timing',
  config: {
    duration: 200,
    easing: Easing.linear,
  },
};

const BuyStackNavigator = () => {

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureDirection: 'horizontal',
        gestureEnabled: false,
        transitionSpec: {
          open: config,
          close: config,
        }
      }}>
      <Stack.Screen
        name="BuyTicket"
        component={BuyTicket} />
    </Stack.Navigator>
  );
};

export default BuyStackNavigator;
