import React, {useEffect, useContext, useState} from 'react';
import { LogBox } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import DeviceInfo from 'react-native-device-info';
import NetInfo from '@react-native-community/netinfo';
import { setItem, getItem } from '../utils/appStorage';
import { Context } from '../context/Provider';
import { fetchPostByToken, fetchPost } from '../utils/fetchData';
import apiUrl from '../utils/apiUrl';
import { deleteMultiItems } from '../utils/appStorage';
import { encryptData } from '../utils/encryptData';
import { decryptData } from '../utils/decryptData';

import TabNavigator from './tab/TabNavigator';
import SplashScreen from '../pages/splash/Splash';

const Stack = createStackNavigator();

const AppNavigator = () => {

    const { 
        userInfo,
        lang,
        changeLang,
        changeUserInfo,
        changeBanners,
        changeCountdownData,
        changeLiveSaleData,
        changeLastDrawData,
        changeProfileInfo,
        changeNet
    } = useContext(Context);

    const lanData = {
        language : lang
    }
    let encryptLanData = encryptData(JSON.stringify(lanData));

    const [
        temporary,
        setTemporary
    ] = useState(true);

    useEffect(() => {
        const netListener = NetInfo.addEventListener(state => {
            changeNet(state.isInternetReachable);
        });
        return () => {
            netListener();
        };
    }, []);

    useEffect(() => {
        const ignoreWarns = [
            "EventEmitter.removeListener",
            "[fuego-swr-keys-from-collection-path]",
            "Setting a timer for a long period of time",
            "ViewPropTypes will be removed from React Native",
            "AsyncStorage has been extracted from react-native",
            "exported from 'deprecated-react-native-prop-types'.",
            "Non-serializable values were found in the navigation state.",
            "VirtualizedLists should never be nested inside plain ScrollViews",
            'Warning: ...',
            'Looks like',
            'ViewPropTypes '
          ];
        
          const warn = console.warn;
          console.warn = (...arg) => {
            for (const warning of ignoreWarns) {
              if (arg[0].startsWith(warning)) {
                return;
              }
            }
            warn(...arg);
          };
        LogBox.ignoreLogs(ignoreWarns); // Ignore log notification by message //['Warning: ...']
        LogBox.ignoreAllLogs();//Ignore all log notifications
        
    }, [])

    useEffect(() => {
        const getInitialData = async () => {
            const response = await fetchPost(apiUrl.banners, {data: encryptLanData});
            if (response?.success && response?.data?.data) {
                const bannerData = JSON.parse(decryptData(response.data.data));
                changeBanners(bannerData?.banner_urls);
                await setItem('@exchange_rate', bannerData?.exchange_rate + '');
                await setItem('@offline_banners', JSON.stringify(bannerData?.banner_urls));
            }

            const countdownResponse = await fetchPost(apiUrl.countdown, {data: encryptLanData});
            if (countdownResponse?.success && countdownResponse?.data?.data) {
                const decCountDown = JSON.parse(decryptData(countdownResponse.data.data));
                changeCountdownData(decCountDown);
                await setItem('@offline_countdown', JSON.stringify(decCountDown));
            }

            const liveTicketsResponse = await fetchPost(apiUrl.liveTickets, {data: encryptLanData});
            if (liveTicketsResponse?.success && liveTicketsResponse?.data?.data) {
                const decLive= JSON.parse(decryptData(liveTicketsResponse.data.data));
                changeLiveSaleData(decLive);
                await setItem('@offline_liveTickets', JSON.stringify(decLive));
            }

            const lastDrawResponse = await fetchPost(apiUrl.lastDrawTickets, {data: encryptLanData});
            if (lastDrawResponse?.success && lastDrawResponse?.data?.data) {
                const decLast = JSON.parse(decryptData(lastDrawResponse.data.data));
                changeLastDrawData(decLast);
                await setItem('@offline_lastDraw', JSON.stringify(decLast));
            }

            setTemporary(false);
        }
        
       getInitialData();

    }, []);

    useEffect(() => {
        const getProfileData = async () => {
            if (userInfo && userInfo.access_token) {
                let encryptLanData2 = encryptData(JSON.stringify(lanData), userInfo.secret_key);

                const response = await fetchPostByToken(apiUrl.profile, {data: encryptLanData2}, userInfo.access_token);
                if (response?.success && response?.data) {
                    const decUserInfo = JSON.parse(decryptData(response.data, userInfo.secret_key));
                    
                    changeProfileInfo(decUserInfo.user);
                    await setItem('@offline_profile', JSON.stringify(decUserInfo.user));
                } else {
                    if (response?.status === 401) {
                        // Unauthenticated
                        logoutHandler();
                    }
                }
            } 
        }
        getProfileData();
    }, [userInfo]);

    const logoutHandler = async () => {
        await deleteMultiItems(['@userInfo']);
        changeUserInfo(null);
    }

    useEffect(() => {
        const getStorageData = async () => {
            getDeviceInfo();
            const language = await getItem('@lang');
            const userInfo = await getItem('@userInfo');

            const offlineBanners = await getItem('@offline_banners');
            const offlineCountdown = await getItem('@offline_countdown');
            const offlineLive = await getItem('@offline_liveTickets');
            const offlineLastDraw = await getItem('@offline_lastDraw');
            const offlineProfile = await getItem('@offline_profile');

            const parsedBanners = JSON.parse(offlineBanners);
            const parsedCountdown = JSON.parse(offlineCountdown);
            const parsedLive = JSON.parse(offlineLive);
            const parsedLastDraw = JSON.parse(offlineLastDraw);
            const parsedProfile = JSON.parse(offlineProfile);

            changeBanners(parsedBanners);
            changeCountdownData(parsedCountdown);
            changeLiveSaleData(parsedLive);
            changeLastDrawData(parsedLastDraw);
            changeProfileInfo(parsedProfile)

            changeLang(language ? language : 'en');
            changeUserInfo(JSON.parse(userInfo));
        }

        getStorageData();
    }, []);

    const getDeviceInfo = () => {

        let _device_model = DeviceInfo.getModel();
        let _os_version = DeviceInfo.getSystemVersion();
        let _app_version = DeviceInfo.getVersion();
        let _os_type = DeviceInfo.getSystemName(); // Android? iOS?

        DeviceInfo.getDeviceName().then((deviceName) => {
            const info = JSON.stringify({
                _device_name : deviceName,
                _device_model,
                _os_version,
                _os_type,
                _app_version
            });
 
            setItem('@deviceInfo', info);
        });
    };
    
    if (temporary) 
        return <NavigationContainer>
            <Stack.Navigator
                screenOptions={{
                    headerShown: false
                }}>
                <Stack.Screen 
                    name="Splash" 
                    component={SplashScreen} />
            </Stack.Navigator>
     
        </NavigationContainer>

    
    return <TabNavigator />
}

export default AppNavigator;